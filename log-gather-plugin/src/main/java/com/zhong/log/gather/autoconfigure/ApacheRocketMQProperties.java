package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhongyumin
 * @date 2021/6/15-下午10:19
 */
@ConfigurationProperties(prefix = "log.gather.producer.apache.rocket")
@Getter
@Setter
public class ApacheRocketMQProperties {
    /**
     * Name Server地址
     */
    private String nameSrvAddr;
}
