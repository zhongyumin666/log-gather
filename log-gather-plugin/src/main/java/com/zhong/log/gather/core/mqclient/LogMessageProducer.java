package com.zhong.log.gather.core.mqclient;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.enums.LogTag;
import com.zhong.log.gather.common.util.GzipUtil;

import java.nio.charset.StandardCharsets;

/**
 * @author zhongyumin
 * @date 2021/1/12-下午2:22
 */
public interface LogMessageProducer {
    void  send(LogMessage logMessage);
    void  start() throws Exception;
    void  shutdown() throws Exception;

    default  byte[]  compress(LogMessage logMessage,int threshold){
        byte[] bytes = logMessage.getMessage();
        byte[] targetBytes;
        //大于byte的进行压缩
        if (bytes.length > threshold) {
            byte[] zipBytes = GzipUtil.zip(bytes);
            logMessage.setLogTag(LogTag.GZIP);
            logMessage.setMessage(zipBytes);
            targetBytes = JSON.toJSONString(logMessage).getBytes(StandardCharsets.UTF_8);
        } else {
            logMessage.setLogTag(LogTag.COMMON);
            targetBytes = JSON.toJSONString(logMessage).getBytes(StandardCharsets.UTF_8);
        }
        return  targetBytes;
    }
}
