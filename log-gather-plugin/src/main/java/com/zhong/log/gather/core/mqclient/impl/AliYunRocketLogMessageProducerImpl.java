package com.zhong.log.gather.core.mqclient.impl;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.impl.rocketmq.ONSUtil;
import com.aliyun.openservices.ons.api.impl.rocketmq.ProducerImpl;
import com.zhong.log.gather.autoconfigure.AliYunRocketMQProperties;
import com.zhong.log.gather.autoconfigure.MQProducerConfigProperties;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

/**
 * @author zhongyumin
 * @date 2021/6/9-下午9:30
 */
public class AliYunRocketLogMessageProducerImpl implements LogMessageProducer {
    @Autowired
    MQProducerConfigProperties mqProducerConfigProperties;
    @Autowired
    AliYunRocketMQProperties aliyunRocketMQProperties;
    Producer producer;
    @Override
    public void send(LogMessage logMessage) {
        try {
            if(Objects.isNull(logMessage))return;
            Message message =new Message(mqProducerConfigProperties.getTopic(),"", JSON.toJSONString(logMessage).getBytes(StandardCharsets.UTF_8));
            producer.sendOneway(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        Properties properties=new Properties();
        properties.setProperty(PropertyKeyConst.AccessKey,aliyunRocketMQProperties.getAccessKey());
        properties.setProperty(PropertyKeyConst.SecretKey,aliyunRocketMQProperties.getSecretKey());
        properties.setProperty(PropertyKeyConst.NAMESRV_ADDR,aliyunRocketMQProperties.getNameSrvAddr());
        properties.setProperty(PropertyKeyConst.GROUP_ID,"GID_LOG_GATHER_AILI_ROCKET");
        ProducerImpl producer = new ProducerImpl(ONSUtil.extractProperties(properties));
        producer.getDefaultMQProducer().setRetryTimesWhenSendAsyncFailed(3);
        producer.getDefaultMQProducer().setRetryTimesWhenSendFailed(3);
        producer.getDefaultMQProducer().setRetryAnotherBrokerWhenNotStoreOK(true);
        this.producer=producer;
        this.producer.start();
    }

    @Override
    public void shutdown() {
        producer.shutdown();
    }
}
