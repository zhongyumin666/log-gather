package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhongyumin
 * @date 2021/3/2-下午10:01
 */
@ConfigurationProperties(prefix = "log.gather.queue")
@Setter
@Getter
public class AmqpAsyncAppenderProperties {
    /**
     * 提交异步队列是否阻塞
     */
    private boolean neverBlock;
    /**
     * 队列大小(和api日志收集一样大小)
     */
    private int queueSize=5000;
    /**
     * logback生效:当异步队列剩余容量低于100时丢弃消息(日志等级为info以下才会丢弃)尽量保证当系统日志量激增时候能多传递info级别以上日志
     * log4j2不生效: 需要配置队列满当策略以达到高性能目的 具体参考源代码 AsyncQueueFullPolicyFactory.create
     * @AsyncQueueFullPolicyFactory.create 代码里面容易影响全局，因为它是使用 系统变量或配置文件指定的properties 创建的拒绝策略
     */
    private int discardingThreshold=4500;
    /**
     * 设置最大调用栈深度
     */
    private int maxCallerDataDepth=10;
}
