package com.zhong.log.gather.core.jvm;

import com.alibaba.fastjson.JSON;
import com.sun.management.GcInfo;
import com.sun.management.UnixOperatingSystemMXBean;
import com.zhong.log.gather.autoconfigure.ApplicationProperties;
import com.zhong.log.gather.autoconfigure.JvmProperties;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.dto.JvmMessageDto;
import com.zhong.log.gather.common.enums.LogLevel;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.management.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * @author zhongyumin
 * @date 2021/3/16-下午10:10
 */
@Slf4j
public class JvmLogGather {
    private static SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    JvmProperties jvmProperties;
    @Autowired
    LogMessageProducer logMessageProducer;
    ScheduledExecutorService executorService;
    private  static  int count =0;
    //垃圾垃圾收集器对应的收集次数
    private static  Map<String,Long> collectors=new HashMap<>();
    public void start(){
        ThreadFactory threadFactory=(r)->{
            Thread t = new Thread(new ThreadGroup("jvm-log-gather"), r,
                    "jvm-log-gather" ,
                    0);
            if (t.isDaemon())
                t.setDaemon(true);
//            if (t.getPriority() != Thread.NORM_PRIORITY)
//                t.setPriority(Thread.NORM_PRIORITY);
            return t;
        };
        //初始化垃圾收集器的初始gc info
        List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();
        garbageCollectorMXBeans.stream().forEach(i->{
            collectors.put(i.getName(),i.getCollectionCount());
        });

        executorService= Executors.newScheduledThreadPool(1,threadFactory);
        //两分钟后开始收集
        executorService.schedule(this::jvmLogGather,2, TimeUnit.MINUTES);
    }
    private void  jvmLogGather(){
        long startTime = System.currentTimeMillis();
        //打印gc
        JvmMessageDto jvmMessageDto = buildBaseJvmMessageDto();
        jvmMessageDto.setGatherCount(count);
        jvmMessageDto.setData(this.buildLogData(jvmMessageDto).toString());
        LogMessage logMessage=new LogMessage();
        logMessage.setLogType(LogType.JVM);
        logMessage.setMessage(JSON.toJSONString(jvmMessageDto).getBytes(StandardCharsets.UTF_8));
        logMessageProducer.send(logMessage);
        log.debug("jvm日志收集花费时间->{}ms",System.currentTimeMillis()-startTime);
        //提交任务
        count++;
        executorService.schedule(this::jvmLogGather,jvmProperties.getJvmInterval(), TimeUnit.SECONDS);
    }
    public void shutdown(){
        executorService.shutdown();
    }

    private StringBuilder buildLogData(JvmMessageDto jvmMessageDt){
        StringBuilder sb=new StringBuilder();
        if((jvmProperties.isSystem()&&operatingSystemMXBean(sb))
                |(jvmProperties.isClazz()&&classLoadingMXBean(sb))
                |(jvmProperties.isJit()&&compilationMXBean(sb))
                |(jvmProperties.isRuntime()&&runtimeMXBean(sb))
                |(jvmProperties.isMemory()&&memoryMXBean(sb))
                |(jvmProperties.isGarbage()&&garbageCollectorMXBeans(sb))
                |(jvmProperties.isMemoryPool()&&memoryPoolMXBeans(sb))
                |(jvmProperties.isThread()&&threadMXBeans(sb))
                |(jvmProperties.isAllThread()&&allThread(sb))){
            jvmMessageDt.setLogLevel(LogLevel.ERROR.name());
        }else {
            jvmMessageDt.setLogLevel(LogLevel.INFO.name());
        }
        //操作系统
        //类加载相关
        //JIT编译器相关
        //应用相关
        //内存相关
        //垃圾搜集相关
        //内存池相关
        //线程相关
        return sb;
    }
    /**
     * 内存池相关
     *
     * @param sb
     */
    private static boolean memoryPoolMXBeans(StringBuilder sb) {
        List<MemoryPoolMXBean> memoryPoolMXBeans = ManagementFactory.getMemoryPoolMXBeans();
        sb.append("<br/>-------------内存池相关-------------<br/>");
        for (MemoryPoolMXBean memoryPoolMXBean : memoryPoolMXBeans) {
            MemoryType type = memoryPoolMXBean.getType();
            String name = memoryPoolMXBean.getName();
            String[] memoryManagerNames = memoryPoolMXBean.getMemoryManagerNames();
            MemoryUsage peakUsage = memoryPoolMXBean.getPeakUsage();
            MemoryUsage usage = memoryPoolMXBean.getUsage();
            sb.append("内存池类型&nbsp;:&nbsp;").append(type.toString()).append("<br/>");
            sb.append("内存池名称&nbsp;:&nbsp;").append(name).append("<br/>");
            sb.append("内存管理器名称&nbsp;:&nbsp;").append(String.join(",", memoryManagerNames)).append("<br/>");
            if (peakUsage != null) {
                sb.append("<em style='color:red'>峰值内存数据</em>&nbsp:&nbsp;")
                  .append(peakUsage.toString()).append("<br/>");
            }
            if (usage != null) {
                sb.append("<em style='color:red'>当前内存数据</em>&nbsp:&nbsp;")
                        .append(usage.toString()).append("<br/>");
            }
            sb.append("<br/>");
        }
        return false;
    }

    /**
     *  所有线程
     * @param sb
     * @return
     */
   private static boolean  allThread(StringBuilder sb){
       ThreadMXBean tmx = ManagementFactory.getThreadMXBean();
       long[] allThreadIds = tmx.getAllThreadIds();
       ThreadInfo[] threadInfo1 = tmx.getThreadInfo(allThreadIds);
       sb.append("所有线程相关信息&nbsp;:&nbsp;");
       for (ThreadInfo threadInfo : threadInfo1) {
           threadInfoLog(threadInfo, sb);
       }
       return false;
   }
    /**
     * 线程相关
     *
     * @param sb
     */
    private static boolean threadMXBeans(StringBuilder sb) {
        ThreadMXBean tmx = ManagementFactory.getThreadMXBean();
        int peakThreadCount = tmx.getPeakThreadCount();
        int threadCount = tmx.getThreadCount();
        int daemonThreadCount = tmx.getDaemonThreadCount();
        long totalStartedThreadCount = tmx.getTotalStartedThreadCount();
        sb.append("<br/>-------------虚拟机线程相关-------------<br/>");
        sb.append("当前线程数&nbsp;:&nbsp;").append(threadCount).append("<br/>");
        sb.append("守护线程数&nbsp;:&nbsp;").append(daemonThreadCount).append("<br/>");
        sb.append("虚拟机启动至今已经启动线程数&nbsp;:&nbsp;").append(totalStartedThreadCount).append("<br/>");
        sb.append("线程数峰值&nbsp;:&nbsp;").append(peakThreadCount).append("<br/>");
        long[] deadlockedThreads = tmx.findDeadlockedThreads();
        if (deadlockedThreads != null) {
            sb.append("<em style='color:red'>死锁线程信息&nbsp;</em>:&nbsp;");
            ThreadInfo[] threadInfos = tmx.getThreadInfo(deadlockedThreads);
            for (ThreadInfo threadInfo : threadInfos) {
                threadInfoLog(threadInfo, sb);
            }
        }
        if(deadlockedThreads!=null&&deadlockedThreads.length>0){
            return true;
        }
        return false;
    }
    private static void threadInfoLog(ThreadInfo threadInfo, StringBuilder sb) {
        long threadId = threadInfo.getThreadId();
        String threadName = threadInfo.getThreadName();
        Thread.State threadState = threadInfo.getThreadState();
        long blockedCount = threadInfo.getBlockedCount();
        long blockedTime = threadInfo.getBlockedTime();
        MonitorInfo[] lockedMonitors = threadInfo.getLockedMonitors();
        LockInfo[] lockedSynchronizers = threadInfo.getLockedSynchronizers();
        LockInfo lockInfo = threadInfo.getLockInfo();
        long lockOwnerId = threadInfo.getLockOwnerId();
        String lockOwnerName = threadInfo.getLockOwnerName();
        StackTraceElement[] stackTraces = threadInfo.getStackTrace();
        sb.append("&nbsp;<br/>&nbsp;【线程id&nbsp;:&nbsp;").append(threadId).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;线程名&nbsp;:&nbsp;").append(threadName).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;线程状态&nbsp;:&nbsp;").append(threadState.toString()).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;线程阻塞总次数&nbsp;:&nbsp;").append(blockedCount).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;线程阻塞总时间&nbsp;:&nbsp;").append(blockedTime / 1000d).append("s<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;锁持有的线程id&nbsp;:&nbsp;").append(lockOwnerId).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;锁持有的线程名&nbsp;:&nbsp;").append(lockOwnerName).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;锁监视器信息&nbsp;:&nbsp;").append(String.join(",", Arrays.toString(lockedMonitors))).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;锁同步器信息&nbsp;:&nbsp;").append(String.join(",", Arrays.toString(lockedSynchronizers))).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;锁名称&nbsp;:&nbsp;").append(lockInfo == null ? null : lockInfo.getClassName()).append("<br/>");
        sb.append("&nbsp;&nbsp;&nbsp;栈帧&nbsp;:&nbsp;");
        for (StackTraceElement stackTrace : stackTraces) {
            sb.append("&nbsp;at&nbsp;" + stackTrace.toString() + "<br/>");
        }
        if (stackTraces == null || stackTraces.length == 0) sb.append("null");
        sb.append("】<br/>");
    }

    /**
     * 垃圾收集相关
     *
     * @param sb
     */
    private static boolean garbageCollectorMXBeans(StringBuilder sb) {
        List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();
        sb.append("<br/>-------------垃圾收集器相关-------------<br/>");
        for (GarbageCollectorMXBean garbageCollectorMXBean : garbageCollectorMXBeans) {
            long collectionCount = garbageCollectorMXBean.getCollectionCount();
            long collectionTime = garbageCollectorMXBean.getCollectionTime();
            String name = garbageCollectorMXBean.getName();
            if(collectionCount==0L||collectors.get(name).equals(collectionCount)){
                continue;
            }
            String memoryPoolNames = String.join(",", garbageCollectorMXBean.getMemoryPoolNames());
            sb.append("<em style='color:red' >垃圾收集器名称&nbsp;</em>:&nbsp").append(name).append("<br/>");
            sb.append("<em style='color:red' >回收区域&nbsp;</em>:&nbsp;").append(memoryPoolNames).append("<br/>");
            sb.append("<em style='color:red' >收集次数&nbsp;</em>:&nbsp;").append(collectionCount).append("<br/>");
            sb.append("<em style='color:red' >收集耗时&nbsp;</em>:&nbsp;").append(collectionTime/1000d).append("s<br/>");
            sb.append("<em style='color:red' >上一次GC收集详情&nbsp;</em>:&nbsp;");
            if(garbageCollectorMXBean instanceof  com.sun.management.GarbageCollectorMXBean){
                com.sun.management.GarbageCollectorMXBean collectorMXBean =(com.sun.management.GarbageCollectorMXBean)garbageCollectorMXBean;
                GcInfo lastGcInfo = collectorMXBean.getLastGcInfo();
                sb.append("&nbsp;").append("GC ID&nbsp;:&nbsp;").append(lastGcInfo.getId()).append("<br/>");
                sb.append("&nbsp;").append("GC 开始时间点&nbsp;:&nbsp;").append(lastGcInfo.getStartTime()).append("<br/>");
                sb.append("&nbsp;").append("GC 结束时间点&nbsp;:&nbsp;").append(lastGcInfo.getEndTime()).append("<br/>");
                sb.append("&nbsp;").append("<em style='color:red' >GC 花费时间&nbsp;</em>:&nbsp;").append(lastGcInfo.getDuration()).append("ms").append("<br/>");
                sb.append("&nbsp;<em style='color:blue' >last&nbsp;before&nbsp;GC&nbsp;</em>:<br/>");
                Map<String, MemoryUsage> memoryUsageBeforeGc = lastGcInfo.getMemoryUsageBeforeGc();
                memoryUsageBeforeGc.forEach((key,value)->{
                   sb.append("&nbsp;&nbsp;").append(key).append("&nbsp;:&nbsp;").append(value.toString()).append("<br/>");
                });
                Map<String, MemoryUsage> memoryUsageAfterGc = lastGcInfo.getMemoryUsageAfterGc();
                sb.append("&nbsp;<em style='color:blue' >last&nbsp;after&nbsp;GC&nbsp;</em>:<br/>");
                memoryUsageAfterGc.forEach((key,value)->{
                    sb.append("&nbsp;&nbsp;").append(key).append("&nbsp;:&nbsp;").append(value.toString()).append("<br/>");
                });
            }
        }
        return false;
    }
    /**
     * 虚拟机当前内存相关
     *
     * @param sb
     */
    private static boolean memoryMXBean(StringBuilder sb) {
        MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
        sb.append("<br/>----------------虚拟机内存相关---------------<br/>");
        MemoryUsage heapMemoryUsage = memoryMXBean.getHeapMemoryUsage();
        long committed = heapMemoryUsage.getCommitted();
        long init = heapMemoryUsage.getInit();
        long max = heapMemoryUsage.getMax();
        long used = heapMemoryUsage.getUsed();
        sb.append("heap&nbsp;memory&nbsp;:&nbsp;").append(heapMemoryUsage.toString()).append("<br/>");
        MemoryUsage nonHeapMemoryUsage = memoryMXBean.getNonHeapMemoryUsage();
        sb.append("non&nbsp;heap&nbsp;memory&nbsp;:&nbsp;").append(nonHeapMemoryUsage.toString()).append("<br/>");
      return false;
    }

    /**
     * 应用相关
     */
    private static  boolean runtimeMXBean(StringBuilder sb){
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        sb.append("<br/>---------------虚拟机基础信息相关--------------<br/>");
        long startTime = runtimeMXBean.getStartTime();
        long uptime = runtimeMXBean.getUptime();
        sb.append("<em style='color:red'>启动时间&nbsp;</em>:&nbsp;").append(dateFormat.format(new Date(startTime))).append("<br/>");
        sb.append("<em style='color:red'>运行时长&nbsp;</em>:&nbsp;").append(uptime/1000d).append("s<br/>");
        //a第一次才打印这种信息
        if(count==1){
        String name = runtimeMXBean.getName();
        String specName = runtimeMXBean.getSpecName();
        String specVendor = runtimeMXBean.getSpecVendor();
        String specVersion = runtimeMXBean.getSpecVersion();
        String managementSpecVersion = runtimeMXBean.getManagementSpecVersion();
        String vmName = runtimeMXBean.getVmName();
        String vmVendor = runtimeMXBean.getVmVendor();
        String vmVersion = runtimeMXBean.getVmVersion();
        sb.append("运行时虚拟机名称&nbsp;:&nbsp;").append(name).append("<br/>");
        sb.append("虚拟机规范名称&nbsp;:&nbsp;").append(name).append("<br/>");
        sb.append("虚拟机规范供应商&nbsp;:&nbsp;").append(specVendor).append("<br/>");
        sb.append("虚拟机规范版本&nbsp;:&nbsp;").append(specVersion).append("<br/>");
        sb.append("虚拟机管理规范版本&nbsp;:&nbsp;").append(managementSpecVersion).append("<br/>");
        sb.append("虚拟机实现名称&nbsp;:&nbsp;").append(vmName).append("<br/>");
        sb.append("虚拟机实现供应商&nbsp;:&nbsp;").append(vmVendor).append("<br/>");
        sb.append("虚拟机实现版本&nbsp;:&nbsp;").append(vmVersion).append("<br/>");
        sb.append("\n--------------虚拟机运行时相关----------------<br/>");
        String bootClassPath = runtimeMXBean.getBootClassPath();
        String classPath = runtimeMXBean.getClassPath();
        List<String> inputArguments = runtimeMXBean.getInputArguments();
        sb.append("<em style='color:red' >输入参数</em>&nbsp;:&nbsp;<br/>");
        if (inputArguments!=null&&inputArguments.size()>0){
            for (String s :inputArguments)
                sb.append("&nbsp;&nbsp;&nbsp;").append(s).append("<br/>");
        }
        String[] split = classPath.split(":");
        sb.append("应用类jar总数&nbsp;:&nbsp;").append(split.length).append("<br/>");
        for (String s:split){
            sb.append("&nbsp;&nbsp;&nbsp;").append(s).append("<br/>");
        }
        String[] split1 = bootClassPath.split(":");
        sb.append("启动类jar总数&nbsp;:&nbsp;").append(split.length).append("<br/>");
        for (String s:split1){
            sb.append("&nbsp;&nbsp;&nbsp;").append(s).append("<br/>");
        }
        }
        return false;
    }
    /**
     * 编译器相关
     *
     * @param sb
     */
    public static boolean compilationMXBean(StringBuilder sb) {
        CompilationMXBean compilationMXBean = ManagementFactory.getCompilationMXBean();
        sb.append("<br/>---------------JIT编译相关--------------<br/>");
        String name = compilationMXBean.getName();
        long totalCompilationTime = compilationMXBean.getTotalCompilationTime();
        sb.append("即时编译器名称&nbsp;:&nbsp;").append(compilationMXBean.getName()).append("<br/>");
        if (compilationMXBean.isCompilationTimeMonitoringSupported()) {
            sb.append("编译时间总长&nbsp;:&nbsp;").append(totalCompilationTime).append("ms<br/>");
        }
        return false;
    }

    /**
     * 类加载相关
     * @param sb
     */
    private static boolean classLoadingMXBean(StringBuilder sb) {
        ClassLoadingMXBean classLoadingMXBean = ManagementFactory.getClassLoadingMXBean();
        sb.append("<br/>---------------类加载相关--------------<br/>");
        int loadedClassCount = classLoadingMXBean.getLoadedClassCount();
        long totalLoadedClassCount = classLoadingMXBean.getTotalLoadedClassCount();
        long unloadedClassCount = classLoadingMXBean.getUnloadedClassCount();
        sb.append("当前已加载类数&nbsp;:&nbsp;").append(loadedClassCount).append("<br/>");
        sb.append("类加载总数&nbsp;:&nbsp;").append(totalLoadedClassCount).append("<br/>");
        sb.append("已经卸载类数&nbsp;:&nbsp;").append(unloadedClassCount).append("<br/>");
        return false;
    }
    /**
     * 操作系统相关
     *
     * @param sb
     */
    private boolean  operatingSystemMXBean(StringBuilder sb) {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        String name = operatingSystemMXBean.getName();
        String arch = operatingSystemMXBean.getArch();
        int availableProcessors = operatingSystemMXBean.getAvailableProcessors();
        double systemLoadAverage = operatingSystemMXBean.getSystemLoadAverage();
        String version = operatingSystemMXBean.getVersion();
        sb.append("<br/>-------------操作系统相关-------------<br/>");
        sb.append("系统名称&nbsp;:").append(name).append("<br/>")
                .append("指令集架构&nbsp;:&nbsp;").append(arch).append("<br/>")
                .append("处理器数&nbsp;:&nbsp;").append(availableProcessors).append("<br/>")
                .append("<em style='color:red' >系统一分钟内平均负载&nbsp;</em>:&nbsp;").append(systemLoadAverage).append("<br/>")
                .append("版本&nbsp;:&nbsp;").append(version).append("<br/>");
        if (operatingSystemMXBean instanceof UnixOperatingSystemMXBean) {
            UnixOperatingSystemMXBean unixOperatingSystemMXBean = (UnixOperatingSystemMXBean) operatingSystemMXBean;
            long descriptorCount = unixOperatingSystemMXBean.getOpenFileDescriptorCount();
            long maxFileDescriptorCount = unixOperatingSystemMXBean.getMaxFileDescriptorCount();
            sb.append("<em style='color:red' >已打开的文件描述符数&nbsp;</em>:&nbsp;").append(descriptorCount).append("<br/>");
            sb.append("<em style='color:red' >最大的文件描述符数&nbsp;</em>:&nbsp;").append(maxFileDescriptorCount).append("<br/>");
            //文件描述符超过90%
            if((descriptorCount/(double)maxFileDescriptorCount)>0.9d){
                return true;
            }
        }
        //系统负载大于80%
        if((systemLoadAverage/(double)availableProcessors)>0.8d){
            return true;
        }
        return false;
    }



    /**
     * 构建基础dto
     * @return
     */
    private JvmMessageDto buildBaseJvmMessageDto(){
        JvmMessageDto jvmMessageDto=new JvmMessageDto();
        jvmMessageDto.setApplicationId(applicationProperties.getApplicationId());
        jvmMessageDto.setApplicationName(applicationProperties.getApplicationName());
        jvmMessageDto.setApplicationVersion(applicationProperties.getApplicationVersion());
        jvmMessageDto.setApplicationDescription(applicationProperties.getApplicationDescription());
        jvmMessageDto.setApplicationHost(applicationProperties.getHost());
        jvmMessageDto.setApplicationPort(String.valueOf(applicationProperties.getServerPort()));
        jvmMessageDto.setEnvironment(applicationProperties.getEnvironment());
        jvmMessageDto.setTimeStamp(System.currentTimeMillis());
        return jvmMessageDto;
    }
}
