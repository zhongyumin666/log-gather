package com.zhong.log.gather.autoconfigure;


import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import com.zhong.log.gather.core.mqclient.impl.AliYunRocketLogMessageProducerImpl;
import com.zhong.log.gather.core.mqclient.impl.ApacheRocketLogMessageProducerImpl;
import com.zhong.log.gather.core.mqclient.impl.KafkaLogMessageProducerImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhongyumin
 * @date 2021/1/12-下午2:05
 */
@Configuration
@EnableConfigurationProperties(MQProducerConfigProperties.class)
@Slf4j
@ConditionalOnEnableLogGather
public class MQProducerAutoConfig {
    @Configuration
    @ConditionalOnEnableLogGather
    @ConditionalOnProperty(prefix = "log.gather.producer",name = "mq-type",havingValue ="ali_yun_rocket_mq" )
    static  class AliYunRocketMqProducerConfig{
      @Bean(initMethod ="start",destroyMethod = "shutdown")
      public LogMessageProducer rocketLogProducer(MQProducerConfigProperties mqProducerConfigProperties){
          LogMessageProducer logMessageProducer =new AliYunRocketLogMessageProducerImpl();
          log.info("加载 aliyun rocketMQ bean 配置成功!");
          return logMessageProducer;
      }
    }
    @Configuration
    @ConditionalOnEnableLogGather
    @ConditionalOnProperty(prefix = "log.gather.producer",name = "mq-type",havingValue ="apache_rocket_mq" )
    static class ApacheRocketMqProducer{
        @Bean(initMethod ="start",destroyMethod = "shutdown")
        public LogMessageProducer rocketLogProducer(MQProducerConfigProperties mqProducerConfigProperties){
            LogMessageProducer logMessageProducer =new ApacheRocketLogMessageProducerImpl();
            log.info("加载 apache rocketMQ producer bean 配置成功!");
            return logMessageProducer;
        }
    }

    @Configuration
    @ConditionalOnEnableLogGather
    @ConditionalOnProperty(prefix = "log.gather.producer",name = "mq-type",havingValue ="apache_kafka" )
    static class ApacheKafkaProducer{
        @Bean(initMethod ="start",destroyMethod = "shutdown")
        public LogMessageProducer rocketLogProducer(MQProducerConfigProperties mqProducerConfigProperties){
            LogMessageProducer logMessageProducer =new KafkaLogMessageProducerImpl();
            log.info("加载 apache kafka producer bean 配置成功!");
            return logMessageProducer;
        }
    }
}
