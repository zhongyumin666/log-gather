package com.zhong.log.gather.core.appender.log4j2;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.dto.LogMessageDto;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.impl.ThrowableProxy;
import org.apache.logging.slf4j.Log4jLoggerFactory;
import org.slf4j.ILoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;

import java.nio.charset.StandardCharsets;

/**
 * @author zhongyumin
 * @date 2021/4/22-下午5:13
 */
@Slf4j
public class SendAppender extends AbstractAppender {
    LogMessageProducer logMessageProducer;
    String applicationName;
    String applicationVersion;
    String applicationDescription;
    String applicationId;
    String serverPort;
    String host;
    String env;

    public SendAppender(LogMessageProducer logMessageProducer) {
        super(SendAppender.class.getName(), null, null, true, null);
        this.logMessageProducer = logMessageProducer;
        ILoggerFactory loggerFactory = StaticLoggerBinder.getSingleton().getLoggerFactory();
        Log4jLoggerFactory log4jLoggerFactory = (Log4jLoggerFactory) loggerFactory;
        org.apache.logging.log4j.core.LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) log4jLoggerFactory.getLoggerContexts().iterator().next();
        this.applicationName = (String) context.getObject("applicationName");
        this.applicationVersion = (String) context.getObject("applicationVersion");
        this.applicationDescription = (String) context.getObject("applicationDescription");
        this.applicationId = (String) context.getObject("applicationId");
        this.serverPort = (String) context.getObject("serverPort");
        this.host = (String) context.getObject("host");
        this.env = (String) context.getObject("env");
    }

    @Override
    public void append(LogEvent event) {
        LogMessageDto logMessageDto=new LogMessageDto();
        logMessageDto.setApplicationId(applicationId);
        logMessageDto.setApplicationName(applicationName);
        logMessageDto.setApplicationVersion(applicationVersion);
        logMessageDto.setApplicationDescription(applicationDescription);
        logMessageDto.setApplicationHost(host);
        logMessageDto.setApplicationPort(serverPort);
        logMessageDto.setLoggerName(event.getLoggerName());
        logMessageDto.setLogLevel(event.getLevel().name());
        logMessageDto.setThreadName(event.getThreadName());
        logMessageDto.setMessage(event.getMessage().getFormattedMessage());
        logMessageDto.setTimeStamp(event.getTimeMillis());
        logMessageDto.setEnvironment(env);
        StringBuilder sb = new StringBuilder();
        this.buildCallData(sb,event.getThrownProxy());
        logMessageDto.setCallerData(sb.toString());
        LogMessage logMessage=new LogMessage();
        byte[] bytes = JSON.toJSONString(logMessageDto).getBytes(StandardCharsets.UTF_8);
        logMessage.setMessage(bytes);
        logMessage.setLogType(LogType.APPLICATION);
        logMessageProducer.send(logMessage);
    }
    private  void   buildCallData(StringBuilder sb, ThrowableProxy e){
        if(e==null)return;
        String className = e.getName();
        String message = e.getMessage();
        sb.append("<em style='color:red'>")
                .append(className)
                .append(":")
                .append(message)
                .append("</em><br/>");
        StackTraceElement[] stackTrace = e.getStackTrace();
        for (StackTraceElement stackTraceElement:stackTrace){
            sb.append(stackTraceElement.toString()+"<br/>");
        }
        buildCallData(sb,e.getCauseProxy());
    }
}
