package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhongyumin
 * @date 2021/3/22-下午11:18
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "log.gather.jvm")
public class JvmProperties {
    //操作系统
    //类加载相关
    //JIT编译器相关
    //应用相关
    //内存相关
    //垃圾搜集相关
    //内存池相关
    //线程相关
    /**
     * 是否开启jvm的日志收集
     */
    private boolean enable;
    /**
     * jvm日志收集的间隔时间(单位/s)
     */
    private int jvmInterval=60;
    private boolean system;
    private boolean clazz;
    private boolean jit;
    private boolean runtime=true;
    private boolean memory;
    private boolean garbage=true;
    private boolean memoryPool=true;
    private boolean allThread;
    private boolean thread=true;
}
