package com.zhong.log.gather.core.listener;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.LoggerContextListener;
import ch.qos.logback.core.Appender;
import com.zhong.log.gather.autoconfigure.AmqpAsyncAppenderProperties;
import com.zhong.log.gather.autoconfigure.ApplicationProperties;
import com.zhong.log.gather.autoconfigure.LogGatherProperties;
import com.zhong.log.gather.core.appender.logback.AmqpAsyncAppender;
import com.zhong.log.gather.core.appender.logback.SendAppender;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.ILoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 注册logback监听器
 *
 * @author zhongyumin
 * @date 2021/1/10-下午9:30
 */
@Slf4j
public class LogBackLogContextRunner implements CommandLineRunner {
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    LogMessageProducer logMessageProducer;
    @Autowired
    AmqpAsyncAppenderProperties amqpAsyncAppenderProperties;
    @Autowired
    LogGatherProperties logGatherProperties;
    @Override
    public void run(String... args) {
        ILoggerFactory loggerFactory = StaticLoggerBinder.getSingleton().getLoggerFactory();
        LoggerContext loggerContext = (LoggerContext) loggerFactory;
        this.new  LogBackLoggerContextListener().reSet(loggerContext);
        log.info("初始化logback LoggerContext数据成功------->");
    }

    private  class LogBackLoggerContextListener implements LoggerContextListener {
        /**
         * 配置文件修改是否回收此监听器,
         *
         * @return @link JMXConfigurator
         */
        @Override
        public boolean isResetResistant() {
            return true;
        }

        @Override
        public void onStart(LoggerContext loggerContext) {
            log.debug("logback LoggerContext onStart数据成功------->");
        }

        @Override
        public void onReset(LoggerContext loggerContext) {
            reSet(loggerContext);
            log.info("logback LoggerContext onReset数据成功------->");
        }

        @Override
        public void onStop(LoggerContext loggerContext) {
            log.debug("LoggerContext onStop数据成功------->");
        }

        @Override
        public void onLevelChange(Logger logger, Level level) {
            log.debug("LoggerContext onLevelChange数据成功------->");
        }

        private synchronized void reSet(LoggerContext loggerContext) {
            loggerContext.putProperty("applicationName", applicationProperties.getApplicationName());
            loggerContext.putProperty("applicationVersion", applicationProperties.getApplicationVersion());
            loggerContext.putProperty("applicationId", applicationProperties.getApplicationId());
            loggerContext.putProperty("applicationDescription", applicationProperties.getApplicationDescription());
            loggerContext.putProperty("serverPort", String.valueOf(applicationProperties.getServerPort()));
            loggerContext.putProperty("host", applicationProperties.getHost());
            loggerContext.putProperty("env", applicationProperties.getEnvironment());
            Logger root = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);
            //设置调用栈深度
            loggerContext.setMaxCallerDataDepth(amqpAsyncAppenderProperties.getMaxCallerDataDepth());
            //注册logback的监听器
            List<LoggerContextListener> copyOfListenerList = loggerContext.getCopyOfListenerList();
            if (!copyOfListenerList.contains(this)) {
                loggerContext.addListener(this);
            }
            try {
                addAppender(root, loggerContext);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        private void addAppender(Logger logger, LoggerContext loggerContext) throws NoSuchFieldException, IllegalAccessException {
            //非ROOT的 或者 (有appender&&不上抛additive=false)
            //原则上不上抛就可以添加自己的appender,但是配了additive=false并且不配置appender说明不需要这里的配置,故也不添加自己的appender
            if (Logger.ROOT_LOGGER_NAME.equals(logger.getName())||(!logger.isAdditive() && logger.iteratorForAppenders().hasNext())) {
                Appender appender = logger.getAppender(AmqpAsyncAppender.class.getName());
                if (appender == null) {
                    //设置初始化RocketMqAppender
                    SendAppender sendAppender = new SendAppender(logMessageProducer);
                    sendAppender.setName(sendAppender.getClass().getName());
                    sendAppender.setContext(loggerContext);
                    //启动
                    sendAppender.start();
                    //设置初始化AmqpAsyncAppender
                    int queueSize = amqpAsyncAppenderProperties.getQueueSize();
                    int discardingThreshold = amqpAsyncAppenderProperties.getDiscardingThreshold();
                    boolean neverBlock = amqpAsyncAppenderProperties.isNeverBlock();
                    AmqpAsyncAppender amqpAsyncAppender = new AmqpAsyncAppender(neverBlock, queueSize, discardingThreshold,Level.toLevel(logGatherProperties.getLevel()));
                    amqpAsyncAppender.setContext(loggerContext);
                    amqpAsyncAppender.addAppender(sendAppender);
                    amqpAsyncAppender.setName(amqpAsyncAppender.getClass().getName());
                    //启动
                    amqpAsyncAppender.start();
                    //添加到根目录
                    logger.addAppender(amqpAsyncAppender);
                } else {
                    AmqpAsyncAppender amqpAsyncAppender = (AmqpAsyncAppender) appender;
                    Appender rocketMqAppender = amqpAsyncAppender.getAppender(SendAppender.class.getName());
                    //若没有appender
                    if (rocketMqAppender == null) {
                        //设置初始化RocketMqAppender
                        SendAppender sendAppender1 = new SendAppender(logMessageProducer);
                        rocketMqAppender.setName(rocketMqAppender.getClass().getName());
                        rocketMqAppender.setContext(loggerContext);
                        //启动
                        rocketMqAppender.start();
                        amqpAsyncAppender.addAppender(sendAppender1);
                    }
                }
            }
                //迭代logback的子logger
                Field field = logger.getClass().getDeclaredField("childrenList");
                field.setAccessible(true);
                List<Logger> childLoggers = (List<Logger>) field.get(logger);
                if (childLoggers != null && childLoggers.size() > 0) {
                    for (Logger logger1 : childLoggers) {
                        addAppender(logger1, loggerContext);
                    }
                }

        }
    }
}
