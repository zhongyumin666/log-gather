package com.zhong.log.gather.core.api.Interceptor;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.autoconfigure.ApplicationProperties;
import com.zhong.log.gather.autoconfigure.LogGatherProperties;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.dto.ApiMessageDto;
import com.zhong.log.gather.common.enums.LogLevel;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.autoconfigure.AmqpAsyncAppenderProperties;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 拦截计算api接口用时拦截日志类
 *
 * @author zhongyumin
 * @date 2021/2/21-上午11:20
 */
@Slf4j
public class ApiInterceptor implements HandlerInterceptor, WebMvcConfigurer, Ordered {
    private ExecutorService executorService;;
    @Autowired
    LogGatherProperties logGatherProperties;
    private final String attrName = "LOG_GATHER_API_START_TIME";
    private ConcurrentHashMap<String, Long> apiCountMap = new ConcurrentHashMap<>();
    @Autowired
    LogMessageProducer logMessageProducer;
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    AmqpAsyncAppenderProperties amqpAsyncAppenderProperties;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        request.setAttribute(attrName, System.currentTimeMillis());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        if (handler instanceof HandlerMethod) {
            Long attribute = (Long) request.getAttribute(attrName);
            long callTime = System.currentTimeMillis() - attribute;
            HandlerMethod method = (HandlerMethod) handler;
            this.doApiLogGather( method.getShortLogMessage(),request,callTime);
        }


    }

    private void doApiLogGather(String name,HttpServletRequest request, long callTime) {
        String uri = request.getRequestURI();
        Map<String, String[]> parameterMap = request.getParameterMap();
        String method = request.getMethod();
        Long count = apiCountMap.get(name);
        if (count == null) {
            apiCountMap.put(name, new Long(1));
            count = 1L;
        } else {
            count = ++count;
            apiCountMap.put(name, count);
        }
        if(callTime<logGatherProperties.getApiMinTime()){
            return;
        }
        ApiMessageDto apiMessageDto = buildBaseApiMessageDto();
        if(callTime>logGatherProperties.getApiMaxTime()){
            apiMessageDto.setLogLevel(LogLevel.ERROR.name());
        }else {
            apiMessageDto.setLogLevel(LogLevel.WARN.name());
        }
        apiMessageDto.setApiName(name);
        apiMessageDto.setCallCount(count);
        apiMessageDto.setCallTime(callTime);
        apiMessageDto.setMethod(method);
        apiMessageDto.setQueryParam(JSON.toJSONString(parameterMap));
        apiMessageDto.setUri(request.getRequestURI());
         executorService.execute(()->{
            LogMessage logMessage=new LogMessage();
            logMessage.setLogType(LogType.API);
            logMessage.setMessage(JSON.toJSONString(apiMessageDto).getBytes(StandardCharsets.UTF_8));
            logMessageProducer.send(logMessage);
        });
    }

    /**
     * 构建基础api message dto
     * @return
     */
    private ApiMessageDto buildBaseApiMessageDto(){
        ApiMessageDto apiMessageDto=new ApiMessageDto();
        apiMessageDto.setApplicationId(applicationProperties.getApplicationId());
        apiMessageDto.setApplicationName(applicationProperties.getApplicationName());
        apiMessageDto.setApplicationVersion(applicationProperties.getApplicationVersion());
        apiMessageDto.setApplicationDescription(applicationProperties.getApplicationDescription());
        apiMessageDto.setApplicationHost(applicationProperties.getHost());
        apiMessageDto.setApplicationPort(String.valueOf(applicationProperties.getServerPort()));
        apiMessageDto.setEnvironment(applicationProperties.getEnvironment());
        apiMessageDto.setTimeStamp(System.currentTimeMillis());
        return apiMessageDto;
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this).addPathPatterns("/**");
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }



    public void start(){
         executorService = new ThreadPoolExecutor(1, 1,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(amqpAsyncAppenderProperties.getQueueSize()),
                runAble -> {
                    Thread t = new Thread(new ThreadGroup("api-log-gather"), runAble,
                            "api-log-gather" ,
                            0);
                    if (t.isDaemon())
                        t.setDaemon(true);
                    return t;
                }, new ThreadPoolExecutor.DiscardPolicy());
    }
    public void shutdown(){
        executorService.shutdown();
    }
}
