package com.zhong.log.gather.core.mqclient.impl;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.autoconfigure.ApacheRocketMQProperties;
import com.zhong.log.gather.autoconfigure.MQProducerConfigProperties;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MQProducer;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * @author zhongyumin
 * @date 2021/6/9-下午9:30
 */
public class ApacheRocketLogMessageProducerImpl implements LogMessageProducer {
    @Autowired
    MQProducerConfigProperties mqProducerConfigProperties;
    @Autowired
    ApacheRocketMQProperties apacheRocketMQProperties;
    MQProducer producer;
    @Override
    public void send(LogMessage logMessage) {
        try {
           if(Objects.isNull(logMessage))return;
            Message message =new Message(mqProducerConfigProperties.getTopic(), JSON.toJSONString(logMessage).getBytes(StandardCharsets.UTF_8));
            producer.sendOneway(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() throws Exception {
        DefaultMQProducer producer=new DefaultMQProducer("LOG_GATHER_APACHE_ROCKET");
        producer.setNamesrvAddr(apacheRocketMQProperties.getNameSrvAddr());
        producer.setRetryTimesWhenSendAsyncFailed(3);
        producer.setRetryAnotherBrokerWhenNotStoreOK(true);
        producer.setRetryTimesWhenSendFailed(3);
        this.producer=producer;
        this.producer.start();
    }

    @Override
    public void shutdown() {
      this.producer.shutdown();
    }

}
