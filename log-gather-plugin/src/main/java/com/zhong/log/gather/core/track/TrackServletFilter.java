package com.zhong.log.gather.core.track;

import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * servlet api 的trackID 注入类
 * @author zhongyumin
 * @date 2021/8/2-下午5:30
 */
public class TrackServletFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String trackId = req.getHeader(TrackUtil.TRACK_ID);
        String sessionId = req.getHeader(TrackUtil.REQUEST_ID);
        if(StringUtils.isEmpty(trackId)){
            MDC.put(trackId, UUID.randomUUID().toString().replace("-",""));
        }
        if(StringUtils.isEmpty(sessionId)){
            MDC.put(sessionId, UUID.randomUUID().toString().replace("-",""));
        }
        try {
            super.doFilter(req, res, chain);
        }catch (Exception e){
            MDC.remove(TrackUtil.TRACK_ID);
            MDC.remove(TrackUtil.REQUEST_ID);
        }
    }
}
