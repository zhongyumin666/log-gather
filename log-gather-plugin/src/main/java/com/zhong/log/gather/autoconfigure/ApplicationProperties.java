package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午2:39
 */
@Getter
@Setter
public class ApplicationProperties {
    @Value("${spring.application.name}")
    private String applicationName;
    @Value("${spring.application.version:1.0}")
    private String applicationVersion;
    @Value("${spring.application.id:1}")
    private String applicationId;
    @Value("${spring.application.description:}")
    private String applicationDescription;
    @Value("${server.port:}")
    private String serverPort;
    @Value("${spring.profiles.active:}")
    private String environment;
    private String host;
    public ApplicationProperties(){
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            this.host = localHost.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
