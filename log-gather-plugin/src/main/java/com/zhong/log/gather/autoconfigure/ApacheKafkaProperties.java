package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhongyumin
 * @date 2021/6/15-下午10:20
 */
@ConfigurationProperties(prefix = "log.gather.producer.apache.kafka")
@Getter
@Setter
public class ApacheKafkaProperties {
    //kafak broker 地址
    private String bootstrapServers;
    //发送到同一个partition的消息会被先存储在batch中，该参数指定一个batch可以使用的内存大小，单位是 byte。不一定需要等到batch被填满才能发送
    private int batchSize=16384;
    // 当linger.ms>0时，延时性会增加，但会提高吞吐量，因为会减少消息发送频率,和batch是相互作用的 1s
    private int lingerMs=1000;
    //生产者的内存缓冲区大小 32m
    private int bufferMemory=33554432;
    //当缓冲区满了最大阻塞时间30s,超过阻塞时间生产者将会抛出异常
    private int maxBlockMs=30000;
    //单个请求发送的最大字节数 1m
    private int maxRequestSize=1048576;
    //失败重试次数
    private int retries=1;
    // <code>none</code>, <code>gzip</code>, <code>snappy</code>, <code>lz4</code>, or <code>zstd</code>
    private String compressionType="gzip";
}
