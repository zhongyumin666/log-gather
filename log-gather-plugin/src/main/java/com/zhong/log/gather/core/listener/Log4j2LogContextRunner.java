package com.zhong.log.gather.core.listener;

import com.zhong.log.gather.autoconfigure.AmqpAsyncAppenderProperties;
import com.zhong.log.gather.autoconfigure.ApplicationProperties;
import com.zhong.log.gather.autoconfigure.LogGatherProperties;
import com.zhong.log.gather.core.appender.log4j2.SendAppender;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.appender.AsyncAppender;
import org.apache.logging.log4j.core.async.ArrayBlockingQueueFactory;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.filter.LevelRangeFilter;
import org.apache.logging.log4j.spi.LoggerContext;
import org.apache.logging.slf4j.Log4jLoggerFactory;
import org.slf4j.ILoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author zhongyumin
 * @date 2021/4/22-下午10:25
 */
@Slf4j
public class Log4j2LogContextRunner implements CommandLineRunner {
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    LogMessageProducer logMessageProducer;
    @Autowired
    AmqpAsyncAppenderProperties amqpAsyncAppenderProperties;
    @Autowired
    LogGatherProperties logGatherProperties;

    @Override
    public void run(String... args) {
        ILoggerFactory loggerFactory = StaticLoggerBinder.getSingleton().getLoggerFactory();
        Log4jLoggerFactory log4jLoggerFactory = (Log4jLoggerFactory) loggerFactory;
        try {

            Field registry = ReflectionUtils.findField(log4jLoggerFactory.getClass(), "registry");
            registry.setAccessible(true);
            Map<LoggerContext, Object> o = (Map<LoggerContext, Object>)registry.get(log4jLoggerFactory);
            Set<LoggerContext> loggerContexts = o.keySet();
            if (!CollectionUtils.isEmpty(loggerContexts)) {
                if (loggerContexts.iterator().next() instanceof org.apache.logging.log4j.core.LoggerContext) {
                    for (LoggerContext ctt:loggerContexts){
                    org.apache.logging.log4j.core.LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) ctt;
                    context.putObject("applicationName", applicationProperties.getApplicationName());
                    context.putObject("applicationVersion", applicationProperties.getApplicationVersion());
                    context.putObject("applicationDescription", applicationProperties.getApplicationDescription());
                    context.putObject("applicationId", applicationProperties.getApplicationId());
                    context.putObject("serverPort", applicationProperties.getServerPort());
                    context.putObject("host", applicationProperties.getHost());
                    context.putObject("env", applicationProperties.getEnvironment());
                    Configuration configuration = context.getConfiguration();
                    setConfig(configuration);
                    context.addPropertyChangeListener(this.new Log4j2ConfigurationListener());
                    }
                } else {
                    log.warn("loggerContext 类型不是 org.apache.logging.log4j.core.LoggerContext 而是{}", loggerContexts.iterator().next().getClass().getCanonicalName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("初始化log4j2 LoggerContext数据成功------->");
    }

    private void setConfig(Configuration configuration) {
        LoggerConfig rootLogger = configuration.getRootLogger();
        //给配置类增加mq的appender(这里不加等级过滤，这个过程交给AsyncAppender去处理)
        SendAppender sendAppender = new SendAppender(logMessageProducer);
        //启动appender
        sendAppender.start();
        configuration.addAppender(sendAppender);
        //构建异步appender
        AsyncAppender asyncAppender = AsyncAppender.newBuilder().setName("log-gather-log4j2-async-appender")
                .setBlocking(amqpAsyncAppenderProperties.isNeverBlock())
                .setBufferSize(amqpAsyncAppenderProperties.getQueueSize())
                .setIgnoreExceptions(true)
                .setConfiguration(configuration)
                .setBlockingQueueFactory(new ArrayBlockingQueueFactory())
                .setAppenderRefs(new AppenderRef[]{AppenderRef.createAppenderRef(SendAppender.class.getName(), null, null)})
                .build();

        //启动appender
        asyncAppender.start();
        configuration.addAppender(asyncAppender);
        //给实际活跃的logger添加额外的appender
        Collection<LoggerConfig> values = configuration.getLoggers().values();
        for (LoggerConfig loggerConfig : values) {
            //异步日志打印的也收集  故注释掉
//           if(loggerConfig instanceof AsyncLoggerConfig){
//               continue;
//           }
            loggerConfig.start();
            //(不上抛或者是root logger)并且不存在此async appender
            if ((!loggerConfig.isAdditive() || "".equals(loggerConfig.getName())) && !loggerConfig.getAppenders().containsKey("log-gather-log4j2-async-appender")) {
                //添加异步appender的范围等级过滤器
                loggerConfig.addAppender(asyncAppender, loggerConfig.getLevel(), LevelRangeFilter.createFilter(Level.FATAL, Level.toLevel(logGatherProperties.getLevel()), Filter.Result.ACCEPT, Filter.Result.DENY));
            }
        }
    }


    private class Log4j2ConfigurationListener implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(org.apache.logging.log4j.core.LoggerContext.PROPERTY_CONFIG)) {
                Configuration newValue = (Configuration) evt.getNewValue();
                setConfig(newValue);
                log.warn("log4j2 配置被改变------->已成功重新装配appender");
            }
        }
    }
}
