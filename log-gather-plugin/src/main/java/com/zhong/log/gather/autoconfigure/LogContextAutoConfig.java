package com.zhong.log.gather.autoconfigure;

import com.zhong.log.gather.core.listener.Log4j2LogContextRunner;
import com.zhong.log.gather.core.listener.LogBackLogContextRunner;
import org.apache.logging.slf4j.Log4jLoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhongyumin
 * @date 2021/1/12-下午6:30
 */
@Configuration
@ConditionalOnEnableLogGather
@EnableConfigurationProperties(AmqpAsyncAppenderProperties.class)
@AutoConfigureAfter(MQProducerAutoConfig.class)
public class LogContextAutoConfig {
    @Configuration
    @ConditionalOnEnableLogGather
    @ConditionalOnClass(ch.qos.logback.classic.LoggerContext.class)
    public static class LogbackContextAutoconfig{
        @Bean
        public LogBackLogContextRunner logBackLogContextRunner() {
            return new LogBackLogContextRunner();
        }
    }
    @Configuration
    @ConditionalOnEnableLogGather
    @ConditionalOnClass(Log4jLoggerFactory.class)
    public static class Log4j2ContextAutoconfig{
        @Bean
        public Log4j2LogContextRunner log4j2LogContextRunner() {
            return new Log4j2LogContextRunner();
        }
    }
}
