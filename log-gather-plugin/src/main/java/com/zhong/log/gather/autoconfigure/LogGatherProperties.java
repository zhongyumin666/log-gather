package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 开启日志收集最顶端的配置
 * @author zhongyumin
 * @date 2021/3/2-下午9:07
 */
@ConfigurationProperties(prefix = "log.gather")
@Getter
@Setter
public class LogGatherProperties {
      /**
       * 是否开启日志收集
       */
      private boolean enable;
      /**
       * 开启指定等级以上的日志收集,不配置默认使用root的日志等级
       */
      private String level;

      /**
       * 是否开启api的日志收集
       */
      private boolean api;
      /**
       * 超过多少秒的打印严重日志信息(单位/ms)
       */
      private int apiMaxTime=2000;
      /**
       * 低于单位的不打印日志打印日志信息(单位/ms)
       */
      private int apiMinTime=1000;

}
