package com.zhong.log.gather.autoconfigure;

import com.zhong.log.gather.common.enums.MqType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author zhongyumin
 * @date 2021/1/12-下午2:46
 */
@ConfigurationProperties(prefix = "log.gather.producer")
@Getter
@Setter
@EnableConfigurationProperties({AliYunRocketMQProperties.class,ApacheKafkaProperties.class, ApacheRocketMQProperties.class})
public class MQProducerConfigProperties {
    /**
     * 消息主题
     */
    private String topic="LOG_GATHER";
    /**
     * mq类型,默认是apache_kafka
     */
    private MqType mqType=MqType.APACHE_KAFKA;
}
