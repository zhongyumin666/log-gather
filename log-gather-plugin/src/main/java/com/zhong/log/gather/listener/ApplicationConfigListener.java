package com.zhong.log.gather.listener;

import com.zhong.log.gather.autoconfigure.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.boot.web.server.WebServer;
import org.springframework.context.ApplicationListener;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午3:38
 */
@Slf4j
public class ApplicationConfigListener implements ApplicationListener<WebServerInitializedEvent> {
    @Autowired
    ApplicationProperties applicationProperties;
    @Override
    public void onApplicationEvent(WebServerInitializedEvent webServerInitializedEvent) {
        if (webServerInitializedEvent.getSource() instanceof WebServer) {
            applicationProperties.setServerPort(String.valueOf(webServerInitializedEvent.getWebServer().getPort()));
            String env;
            String[] profiles = webServerInitializedEvent.getApplicationContext().getEnvironment().getActiveProfiles();
            if (profiles != null && profiles.length > 0) {
                env = profiles[0];
            } else {
                env = "local";
            }
            applicationProperties.setEnvironment(env);
        }
        log.info("环境配置初始化成功--------->");
    }
}
