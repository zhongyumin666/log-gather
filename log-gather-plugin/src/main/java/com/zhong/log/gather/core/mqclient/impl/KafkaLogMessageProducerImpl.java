package com.zhong.log.gather.core.mqclient.impl;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.autoconfigure.ApacheKafkaProperties;
import com.zhong.log.gather.autoconfigure.ApplicationProperties;
import com.zhong.log.gather.autoconfigure.MQProducerConfigProperties;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Properties;

/**
 * @author zhongyumin
 * @date 2021/6/9-下午9:25
 */
public class KafkaLogMessageProducerImpl implements LogMessageProducer {
    @Autowired
    MQProducerConfigProperties mqProducerConfigProperties;
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    ApacheKafkaProperties apacheKafkaProperties;
    Producer producer;
    @Override
    public void send(LogMessage logMessage) {
        ProducerRecord producerRecord = new ProducerRecord(mqProducerConfigProperties.getTopic(), JSON.toJSONString(logMessage));
        producer.send(producerRecord);
    }

    @Override
    public void start() {
        Properties properties=new Properties();
        properties.put("bootstrap.servers",apacheKafkaProperties.getBootstrapServers());
        properties.put("batch.size",apacheKafkaProperties.getBatchSize());//16k
        properties.put("ack",0);//0无需回应1写入日志all至少写入一个副本
        properties.put("linger.ms", apacheKafkaProperties.getLingerMs());
        properties.put("client.id",applicationProperties.getHost()+":"+applicationProperties.getHost());//用于标识发送消息的客户端，通常用于日志和性能指标以及配额
        properties.put("buffer.memory", apacheKafkaProperties.getBufferMemory());// 32MB
        properties.put("max.block.ms", apacheKafkaProperties.getMaxBlockMs());// 1s
        properties.put("max.request.size", apacheKafkaProperties.getMaxRequestSize());// 1m
        properties.put("retries", apacheKafkaProperties.getRetries());//失败重试次数
        properties.put("compression.type", apacheKafkaProperties.getCompressionType());//数据压缩算法
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String,String> producer=new KafkaProducer(properties);
        this.producer=producer;
    }

    @Override
    public void shutdown() {
        this.producer.close();
    }

}
