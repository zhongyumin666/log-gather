package com.zhong.log.gather.core.appender.logback;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import ch.qos.logback.core.AppenderBase;
import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.dto.LogMessageDto;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.core.mqclient.LogMessageProducer;
import org.slf4j.impl.StaticLoggerBinder;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author zhongyumin
 * @date 2021/1/9-下午11:16
 */
public class SendAppender extends AppenderBase<ILoggingEvent> {
    LogMessageProducer logMessageProducer;
    String applicationName;
    String applicationVersion;
    String applicationDescription;
    String applicationId;
    String serverPort;
    String host;
    String env;
    public SendAppender(LogMessageProducer logMessageProducer){
        this.logMessageProducer = logMessageProducer;
        Map<String, String> propertyMap = ((LoggerContext)StaticLoggerBinder.getSingleton().getLoggerFactory()).getCopyOfPropertyMap();
         this.applicationName = propertyMap.get("applicationName");
        this.applicationVersion =propertyMap.get("applicationVersion");
        this.applicationDescription = propertyMap.get("applicationDescription");
        this.applicationId = propertyMap.get("applicationId");
        this.serverPort = propertyMap.get("serverPort");
        this.host = propertyMap.get("host");
        this.env=propertyMap.get("env");
    }
    @Override
    protected void append(ILoggingEvent iLoggingEvent) {
        LogMessageDto logMessageDto=new LogMessageDto();
        logMessageDto.setApplicationId(applicationId);
        logMessageDto.setApplicationName(applicationName);
        logMessageDto.setApplicationVersion(applicationVersion);
        logMessageDto.setApplicationDescription(applicationDescription);
        logMessageDto.setApplicationHost(host);
        logMessageDto.setApplicationPort(serverPort);
        logMessageDto.setLoggerName(iLoggingEvent.getLoggerName());
        logMessageDto.setLogLevel(iLoggingEvent.getLevel().levelStr);
        logMessageDto.setThreadName(iLoggingEvent.getThreadName());
        logMessageDto.setMessage(iLoggingEvent.getFormattedMessage());
        logMessageDto.setTimeStamp(iLoggingEvent.getTimeStamp());
        logMessageDto.setEnvironment(env);
        if(Level.ERROR.equals(iLoggingEvent.getLevel())||Level.WARN.equals(iLoggingEvent.getLevel())){
            StackTraceElement[] callerData = iLoggingEvent.getCallerData();
            if(callerData!=null&&callerData.length>0){
                //异常栈
                IThrowableProxy throwableProxy = iLoggingEvent.getThrowableProxy();
                StringBuilder sb=new StringBuilder(2048);
                if(throwableProxy!=null){
                    sb.append("<em style='color:red'>").append(throwableProxy.getClassName()).append(":").append(throwableProxy.getMessage()).append("</em><br/>");
                }
                for(StackTraceElement element:callerData){
                    sb.append("at " + element.toString()+"<br/>");
                }
                buildCallData(sb,throwableProxy);
                logMessageDto.setCallerData(sb.toString());
            }
        }
        LogMessage logMessage=new LogMessage();
        byte[] bytes = JSON.toJSONString(logMessageDto).getBytes(StandardCharsets.UTF_8);
        logMessage.setMessage(bytes);
        logMessage.setLogType(LogType.APPLICATION);
        logMessageProducer.send(logMessage);
    }
    private  void   buildCallData(StringBuilder sb,IThrowableProxy e){
        if(e==null)return;
        String className = e.getClassName();
        String message = e.getMessage();
        sb.append("<em style='color:red'>")
                .append(className)
                .append(":")
                .append(message)
                .append("</em><br/>");
        StackTraceElementProxy[] traceElementProxyArray = e.getStackTraceElementProxyArray();
        for (StackTraceElementProxy traceElementProxy:traceElementProxyArray){
            sb.append(traceElementProxy.getSTEAsString()+"<br/>");
        }
        buildCallData(sb,e.getCause());
    }
}
