package com.zhong.log.gather.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhongyumin
 * @date 2021/6/15-下午10:18
 */
@ConfigurationProperties(prefix = "log.gather.producer.aliyun.rocket")
@Getter
@Setter
public class AliYunRocketMQProperties {
    /**
     * AccessKey, 用于标识、校验用户身份(阿里云rocketMq用到)
     */
    private String accessKey;

    /**
     * SecretKey, 用于标识、校验用户身份(阿里云rocketMq用到)
     */
    private String secretKey;
    /**
     * Name Server地址
     */
    private String nameSrvAddr;
}
