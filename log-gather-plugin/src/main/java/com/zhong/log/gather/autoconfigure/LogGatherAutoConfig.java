package com.zhong.log.gather.autoconfigure;

import com.zhong.log.gather.core.api.Interceptor.ApiInterceptor;
import com.zhong.log.gather.core.jvm.JvmLogGather;
import com.zhong.log.gather.listener.ApplicationConfigListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhongyumin
 * @date 2021/3/2-下午9:10
 */
@Configuration
@ConditionalOnEnableLogGather
@Slf4j
public class LogGatherAutoConfig {
    @Bean
    public ApplicationConfigListener applicationConfigListener() {
        return new ApplicationConfigListener();
    }

    @Bean
    public LogGatherProperties logGatherProperties() {
        return new LogGatherProperties();
    }

    @Bean
    public ApplicationProperties applicationProperties() {
        return new ApplicationProperties();
    }

    @Configuration
    @ConditionalOnEnableApiLogGather
    public static class ApiLogAutoConfig {
        @Bean(initMethod = "start",destroyMethod = "shutdown")
        public ApiInterceptor apiInterceptor(LogGatherProperties logGatherProperties) {
            ApiInterceptor apiInterceptor = new ApiInterceptor();
            log.info("api 调用日志拦截器装配成功----->最大忍受调用时间{}ms", logGatherProperties.getApiMaxTime());
            return apiInterceptor;
        }
    }
    @Configuration
    @ConditionalOnEnableJvmLogGather
    public static class JvmLogAutoConfig {
        @Bean
        public JvmProperties jvmProperties() {
            return new JvmProperties();
        }

        @Bean(initMethod = "start",destroyMethod = "shutdown")
        public JvmLogGather jvmLogGather(JvmProperties jvmProperties) {
            JvmLogGather jvmLogGather = new JvmLogGather();
            log.info("jvm 日志收集器装配成功--------每{}秒收集一次", jvmProperties.getJvmInterval());
            return jvmLogGather;
        }
    }
}
