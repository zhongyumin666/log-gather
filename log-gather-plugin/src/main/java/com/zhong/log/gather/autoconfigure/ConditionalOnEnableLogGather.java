package com.zhong.log.gather.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

import java.lang.annotation.*;

/**
 * @author zhongyumin
 * @date 2021/3/2-下午9:31
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ConditionalOnProperty(prefix = "log.gather",name = "enable",havingValue ="true" )
public @interface ConditionalOnEnableLogGather {
}
