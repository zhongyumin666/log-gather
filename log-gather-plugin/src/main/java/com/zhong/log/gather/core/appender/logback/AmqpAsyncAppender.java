package com.zhong.log.gather.core.appender.logback;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * @author zhongyumin
 * @date 2021/1/9-下午10:05
 */
public class AmqpAsyncAppender extends AsyncAppender {
    public AmqpAsyncAppender(boolean neverBlock, int QueueSize, int discardingThreshold, Level level){
        super();
        setNeverBlock(neverBlock);
        //队列大小
        setQueueSize(QueueSize);
        //丢弃阀值
        setDiscardingThreshold(discardingThreshold);
        setIncludeCallerData(true);
        addFilter(new LogGatherLevelFilter(level));
    }
    static class  LogGatherLevelFilter extends Filter<ILoggingEvent> {
        Level level;
        public LogGatherLevelFilter(Level level) {
            this.level=level;
        }
        @Override
        public FilterReply decide(ILoggingEvent event) {
            if(level==null||level.levelInt>event.getLevel().levelInt)return FilterReply.DENY;
            else return FilterReply.ACCEPT;
        }
    }
}
