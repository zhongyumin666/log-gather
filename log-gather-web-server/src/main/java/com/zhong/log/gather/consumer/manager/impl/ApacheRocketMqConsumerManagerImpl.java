package com.zhong.log.gather.consumer.manager.impl;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.config.ApacheRocketMQConsumerProperties;
import com.zhong.log.gather.consumer.adapter.MessageSaveAdapter;
import com.zhong.log.gather.consumer.manager.MqConsumerManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhongyumin
 * @date 2021/3/3-下午4:03
 */
@Slf4j
public class ApacheRocketMqConsumerManagerImpl implements MqConsumerManager, InitializingBean, DisposableBean {
    List<DefaultMQPushConsumer> consumers = new ArrayList<>();
    @Autowired
    MessageSaveAdapter messageSaveAdapter;
    @Autowired
    ApacheRocketMQConsumerProperties properties;
    @Override
    public void start() throws Exception {
        int i = 1;
        for (DefaultMQPushConsumer consumer : consumers) {
            consumer.start();
            log.info("ApacheRocketMq日志消费者{}启动成功-------->", i);
            i++;
        }
    }

    @Override
    public void shutdown() {
        consumers.forEach(item -> {
            item.shutdown();
        });
        log.info("ApacheRocketMq日志消费者关闭成功-------->");
    }



    @Override
    public void destroy()  {
        this.shutdown();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        int count = properties.getCount();
        for (int i=0;i<count;i++){
            DefaultMQPushConsumer mqConsumer = new DefaultMQPushConsumer();
            mqConsumer.setNamesrvAddr(properties.getNameSrvAddr());
            mqConsumer.setConsumerGroup("LOG_GATHER_APACHE_ROCKET");
            mqConsumer.subscribe(properties.getTopic(), "");
            mqConsumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
            mqConsumer.setConsumeTimeout(properties.getConsumeTimeout());
            mqConsumer.setConsumeMessageBatchMaxSize(properties.getPullBatchSize());
            mqConsumer.setPullInterval(properties.getPullInterval());
            mqConsumer.setMaxReconsumeTimes(properties.getMaxReconsumeTimes());
            mqConsumer.setConsumeThreadMin(properties.getConsumeThreadMin());
            mqConsumer.setConsumeThreadMax(properties.getConsumeThreadMax());
            mqConsumer.setVipChannelEnabled(false);
            mqConsumer.setUseTLS(false);
            mqConsumer.setInstanceName("LOG_GATHER-"+i);
            mqConsumer.registerMessageListener((List<MessageExt> msgs, ConsumeConcurrentlyContext context) -> {
                try {
                    List<byte[]> list = msgs.stream().map(Message::getBody).collect(Collectors.toList());
                    List<LogMessage> messages = list.stream().map(item -> (LogMessage) JSON.parseObject(item, LogMessage.class)).collect(Collectors.toList());
                    messageSaveAdapter.submit(messages);
                } catch (Exception e) {
                    log.info("ApacheRocketMq消费失败------>{}", e);
                    return ConsumeConcurrentlyStatus.RECONSUME_LATER;
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            });
            consumers.add(mqConsumer);
        }
        this.start();
    }
}
