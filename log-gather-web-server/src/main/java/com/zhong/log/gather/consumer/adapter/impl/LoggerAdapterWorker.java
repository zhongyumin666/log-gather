package com.zhong.log.gather.consumer.adapter.impl;

import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.service.LogService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 适配工人,承担收集数据批量入库的操作
 *
 * @author zhongyumin
 * @date 2021/7/30-下午4:10
 */
@Slf4j
public class LoggerAdapterWorker extends Thread {
    private static volatile boolean stop = false;
    /**
     * 线程处理的日志类型
     */
    private LogType logType;
    /**
     * 任务队列
     */
    private LinkedBlockingQueue<LogMessage> taskQueue;
    /**
     * 最小批量插入的大小
     */
    private int minSubmitSize;
    /**
     * 最大延迟/ms
     */
    private int maxDelay;
    /**
     * 异步入库线程池
     */
    private ThreadPoolTaskExecutor executorService;
    /**
     * 批量插入实现类
     */
    private LogService logService;

    public LoggerAdapterWorker(LogType logType, String name, int capacity, int minSubmitSize, int maxDelay, ThreadPoolTaskExecutor executorService, LogService logService) {
        this.taskQueue = new LinkedBlockingQueue<>(capacity);
        this.logService = logService;
        this.logType=logType;
        this.minSubmitSize = minSubmitSize;
        this.maxDelay = maxDelay;
        this.executorService = executorService;
        super.setName(name);
        super.setDaemon(true);
    }

    /**
     * 提交任务
     *
     * @param logMessage
     */
    public void submit(LogMessage logMessage) {
        //能扔队列扔到队列,扔不到由调用线程执行插入操作
        if (!stop && taskQueue.offer(logMessage)) {
            logService.batchInsert(logType, Arrays.asList(logMessage));
        }
    }

    /**
     * 线程停止标志位
     */
    public void shutdown(){
        stop = true;
    }

    public void startUp() {
        this.start();
        log.info("{}消息适配线程启动成功----->",logType.getName());
    }

    @SneakyThrows
    @Override
    public void run() {
        try {
            while (!stop) {
                long startTime = System.currentTimeMillis();
                for (; ; ) {
                    List<LogMessage> logMessages = new ArrayList<>(minSubmitSize / 2);
                    //最多等待5ms拉取
                    LogMessage take = taskQueue.poll(5, TimeUnit.MILLISECONDS);
                    if (Objects.nonNull(take) && logMessages.add(take))
                        if ((System.currentTimeMillis() - startTime) > maxDelay || logMessages.size() > minSubmitSize) {
                            executorService.submit(() -> {
                                if (logMessages.size() != 0) {
                                    logService.batchInsert(logType, logMessages);
                                }
                                log.debug("{}{}批量插入{}大小{}", Thread.currentThread().getId(), Thread.currentThread().getName(), logType.getName(), logMessages.size());
                            });
                            break;
                        }
                }
            }
            //清理队列工作
        } catch (InterruptedException e) {
            LogMessage[] logMessages = taskQueue.toArray(new LogMessage[0]);
            int size=0;
            List<LogMessage> msg=new ArrayList<>(minSubmitSize);
            for(LogMessage log: logMessages){
                  msg.add(log);
                  size++;
                  if(msg.size()==minSubmitSize){
                      List<LogMessage> list=msg;
                      executorService.submit(()->{
                          logService.batchInsert(logType,list);
                      });
                      msg=new ArrayList<>(minSubmitSize);
                      size=0;
                  }
            }
        }
        log.info("{}线程停止----------->", Thread.currentThread().getName());
    }
}
