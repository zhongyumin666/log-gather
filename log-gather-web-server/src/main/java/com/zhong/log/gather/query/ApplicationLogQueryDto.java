package com.zhong.log.gather.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhongyumin
 * @date 2021/1/14-下午2:16
 */
@Getter
@Setter
@ApiModel("应用日志查询参数")
public class ApplicationLogQueryDto extends BaseLogQueryDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //消息
    @ApiModelProperty(value="消息",example = "NULL POINT EXCEPTION")
    private String message;
    @ApiModelProperty(value="线程名称",example = "http-nio-9070-exec-183")
    private String threadName;
    //开始时间
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    //结束时间
    @ApiModelProperty("结束时间")
    private Date endTime;
    @ApiModelProperty("每页大小")
    private int pageSize=30;
    @ApiModelProperty("页码")
    private int pageNo=1;

}
