package com.zhong.log.gather.dao.repository;

import com.zhong.log.gather.dao.entity.JvmLogPo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:15
 */
public interface JvmLogRepository extends ElasticsearchRepository<JvmLogPo,Long> {
}
