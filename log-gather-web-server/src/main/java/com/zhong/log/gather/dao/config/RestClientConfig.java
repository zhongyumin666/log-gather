package com.zhong.log.gather.dao.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午11:42
 */
@Configuration
public class RestClientConfig {
    @Value("${spring.elasticsearch.rest.host}")
    private String hostname;
    @Value("${spring.elasticsearch.rest.port}")
    private Integer transport;
    @Value("${spring.elasticsearch.rest.scheme:http}")
    private String scheme;
    @Value("${spring.elasticsearch.rest.username:}")
    private String username;
    @Value("${spring.elasticsearch.rest.password:}")
    private String password;
    @Bean
    public RestHighLevelClient elasticsearchClient() {
//        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
//                .connectedTo("127.0.0.1:9200")
//                .build();
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
        RestClientBuilder builder = RestClient.builder(new HttpHost(hostname, transport, scheme));
        builder.setHttpClientConfigCallback(httpClientBuilder ->
                httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
        return new RestHighLevelClient(builder);
    }
}
