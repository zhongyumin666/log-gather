package com.zhong.log.gather.scheduling;

import cn.hutool.core.date.DateUtil;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.query.LogDeleteDto;
import com.zhong.log.gather.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;

/**
 * @author zhongyumin
 * @date 2021/1/27-下午5:07
 */
@Component
@Slf4j
public class LogCleanScheduling {
    @Autowired
    LogService logService;

    @Scheduled(cron = "0 0 0 * * MON")
    public void clean() {
        //每七天删除一次warn以下日志级别的日志
        LogDeleteDto logDeleteDto = new LogDeleteDto();
        logDeleteDto.setLogLevels(Arrays.asList("DEBUG", "INFO", "TRACE,WARN"));
        logDeleteDto.setEndTime(DateUtil.format(new Date(System.currentTimeMillis() - 1000l * 60 * 60 * 24 * 7), "yyyy-MM-dd HH:mm:ss"));
        for (LogType logType : LogType.values()) {
            try {
                logDeleteDto.setLogType(logType);
                logService.deleteLog(logDeleteDto);
                log.info("定时任务删除{}成功,执行时间=>{}", logType.getName(), DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"));
            } catch (Throwable e) {
                log.error("定时任务删除{}失败,执行时间=>{}", logType.getName(), DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"),e);
            }
        }


    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void cleanDEBUG() {
        //每天删除一次debug以下日志级别的日志
        LogDeleteDto logDeleteDto = new LogDeleteDto();
        logDeleteDto.setLogLevels(Arrays.asList("DEBUG"));
        logDeleteDto.setEndTime(DateUtil.format(new Date(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss"));
        for (LogType logType : LogType.values()) {
            try {
                logDeleteDto.setLogType(logType);
                logService.deleteLog(logDeleteDto);
                log.info("DEBUG定时任务删除{}成功,执行时间=>{}", logType.getName(), DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"));
            } catch (Throwable e) {
                log.error("DEBUG定时任务删除{}失败,执行时间=>{}", logType.getName(), DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"),e);
            }
        }
    }

    @Scheduled(cron = "0 0 0 1/3 * ?")
    public void cleanINFO() {
        //每3天删除一次INFO以下日志级别的日志
        LogDeleteDto logDeleteDto = new LogDeleteDto();
        logDeleteDto.setLogLevels(Arrays.asList("INFO"));
        logDeleteDto.setEndTime(DateUtil.format(new Date(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss"));
        for (LogType logType : LogType.values()) {
            try {
                logDeleteDto.setLogType(logType);
                logService.deleteLog(logDeleteDto);
                log.info("INFO定时任务删除{}成功,执行时间=>{}", logType.getName(), DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"));
            } catch (Throwable e) {
                log.error("INFO定时任务删除{}失败,执行时间=>{}", logType.getName(), DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"),e);
            }
        }
    }
}
