package com.zhong.log.gather.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zhongyumin
 * @date 2021/3/22-下午10:21
 */
@Data
@ApiModel("api日志请求参数")
public class ApiLogQueryDto extends BaseLogQueryDto {
    /**
     * 消息
     */
    private String message;

    private String method;
    //开始时间
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    //结束时间
    @ApiModelProperty("结束时间")
    private Date endTime;
    @ApiModelProperty("每页大小")
    private int pageSize=30;
    @ApiModelProperty("页码")
    private int pageNo=1;
}
