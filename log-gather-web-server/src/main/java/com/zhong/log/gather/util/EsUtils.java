package com.zhong.log.gather.util;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhongyumin
 * @date 2021/3/21-下午1:59
 */
public class EsUtils {

    /**
     *  构建不影响聚合的过滤查询
     * @param fieldValues
     */
    public static BoolQueryBuilder buildBoolTermsConditions(Map<String,Object> fieldValues){
        BoolQueryBuilder builder=new BoolQueryBuilder();
        fieldValues.forEach((key,value)->{
            if(!StringUtils.isEmpty(value)){
                if(value instanceof List){
                    TermsQueryBuilder termsQueryBuilder=new TermsQueryBuilder(key,((List) value).toArray());
                    builder.must(termsQueryBuilder);
                }else {
                    TermQueryBuilder termQueryBuilder=new TermQueryBuilder(key,value);
                    builder.must(termQueryBuilder);
                }

            }
        });
        return builder;
    }
    /**
     * 构建聚合builder
     *
     * @param aggName
     * @param aggSize
     * @param aggField
     * @return
     */
    public static TermsAggregationBuilder buildAgg(String aggName, Integer aggSize, String aggField) {
        TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms(aggName)
                .size(aggSize == null ? 10 : aggSize)
                .field(aggField);
        return termsAggregationBuilder;
    }
    /**
     *  构建聚合结果
     * @param aggregations
     * @return
     */
    public static Map<String, List<String>> getAggSut(Aggregations aggregations){
        Map<String,List<String>> aggSut=new LinkedHashMap<>();
        Map<String, Aggregation> stringAggregationMap = aggregations.asMap();
        stringAggregationMap.forEach((key,value)->{
            Terms terms = (Terms) value;
            List<String> aggValue = terms.getBuckets().stream().filter(item->item.getDocCount()!=0).map(item -> item.getKeyAsString()).collect(Collectors.toList());
            aggSut.put(key,aggValue);
        });
        return aggSut;
    }
}
