package com.zhong.log.gather.query;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/3/23-上午9:53
 */
@Getter
@Setter
public class BaseLogDeleteDto {
    //应用id
    @ApiModelProperty(value = "应用环境",example = "pro")
    private String environment;
    //应用名字
    @ApiModelProperty(value = "应用名字",example = "log-gather-web-test")
    private String applicationName;
    //应用版本
    @ApiModelProperty(value = "应用版本",example = "1.0")
    private String applicationVersion;
    //应用描述
    @ApiModelProperty(value = "应用描述",example = "日志查询测试服务")
    private String applicationDescription;
    //日志等级
    @ApiModelProperty(value = "日志等级",example = "INFO")
    private List<String> logLevels= Arrays.asList("DEBUG","INFO","TRACE");
    //机器地址
    @ApiModelProperty(value = "机器地址",example = "127.0.0.1")
    private String host;
    //机器端口
    @ApiModelProperty(value = "机器端口",example = "机器端口")
    private String port;
    //开始时间
    @ApiModelProperty(value = "开始时间" ,example="2021-12-01 16:00:00")
    private String startTime;
    //结束时间
    @ApiModelProperty(value = "结束时间",example="2021-12-01 16:00:00")
    private String endTime= DateUtil.format(new Date(System.currentTimeMillis()-1000l*60*60*24*3),"yyyy-MM-dd HH:mm:ss");
}
