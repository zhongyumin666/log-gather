
package com.zhong.log.gather.consumer.manager.impl;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.*;
import com.aliyun.openservices.ons.api.batch.BatchConsumer;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.config.AliYunRocketMQConsumerProperties;
import com.zhong.log.gather.consumer.adapter.MessageSaveAdapter;
import com.zhong.log.gather.consumer.manager.MqConsumerManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * @author zhongyumin
 * @date 2021/4/1-上午10:46
 */
@Slf4j
public class AliYunRocketMqConsumerManagerImpl implements MqConsumerManager, InitializingBean, DisposableBean {
    @Autowired
    MessageSaveAdapter messageSaveAdapter;
    List<BatchConsumer> consumers = new ArrayList<>();
    @Autowired
    AliYunRocketMQConsumerProperties properties;
    @Override
    public void start() {
        int i = 1;
        for (BatchConsumer consumer : consumers) {
            consumer.start();
            log.info("AliYunRocket日志消费者{}启动成功-------->", i);
            i++;
        }
    }

    @Override
    public void shutdown() {
        consumers.forEach(item -> {
            item.shutdown();
        });
        log.info("AliYunRocket日志消费者关闭成功-------->");
    }



    @Override
    public void destroy() {
        this.shutdown();
    }

    @Override
    public void afterPropertiesSet()  {
        int count = properties.getCount();
        Properties aliConsumerProperties = new Properties();
        aliConsumerProperties.setProperty(PropertyKeyConst.AccessKey, properties.getAccessKey());
        aliConsumerProperties.setProperty(PropertyKeyConst.SecretKey, properties.getSecretKey());
        aliConsumerProperties.setProperty(PropertyKeyConst.NAMESRV_ADDR, properties.getNameSrvAddr());
        aliConsumerProperties.setProperty(PropertyKeyConst.GROUP_ID, "GID_LOG_GATHER_AILI_ROCKET");
        //每次最大拉取消费数量
        aliConsumerProperties.setProperty(PropertyKeyConst.MAX_BATCH_MESSAGE_COUNT, "1024");
        aliConsumerProperties.setProperty(PropertyKeyConst.ConsumeMessageBatchMaxSize, String.valueOf(properties.getPullBatchSize()));
        aliConsumerProperties.setProperty(PropertyKeyConst.ConsumeTimeout, String.valueOf(properties.getConsumeTimeout()));
        aliConsumerProperties.setProperty(PropertyKeyConst.ConsumeThreadNums, String.valueOf(properties.getConsumeThreadMin()));
        aliConsumerProperties.setProperty(PropertyKeyConst.MaxReconsumeTimes, String.valueOf(properties.getMaxReconsumeTimes()));
        for (int i = 0; i < count; i++) {
            BatchConsumer mqConsumer = ONSFactory.createBatchConsumer(aliConsumerProperties);
            mqConsumer.subscribe(properties.getTopic(), "", (List<Message> msgs, ConsumeContext context) -> {
                try {
                    List<byte[]> list = msgs.stream().map(Message::getBody).collect(Collectors.toList());
                    List<LogMessage> messages = list.stream().map(item -> (LogMessage) JSON.parseObject(item, LogMessage.class)).collect(Collectors.toList());
                    messageSaveAdapter.submit(messages);
                } catch (Exception e) {
                    log.warn("ApacheRocketMq消费失败------>", e);
                    e.printStackTrace();
                    return Action.ReconsumeLater;
                }
                return Action.CommitMessage;
            });
            consumers.add(mqConsumer);
        }
        this.start();
    }
}