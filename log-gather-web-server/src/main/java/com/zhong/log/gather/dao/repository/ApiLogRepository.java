package com.zhong.log.gather.dao.repository;

import com.zhong.log.gather.dao.entity.ApiLogPo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:16
 */
public interface ApiLogRepository extends ElasticsearchRepository<ApiLogPo,Long> {
}
