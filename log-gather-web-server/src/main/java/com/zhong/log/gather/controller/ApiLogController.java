package com.zhong.log.gather.controller;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.query.ApiLogQueryDto;
import com.zhong.log.gather.query.BaseLogDeleteDto;
import com.zhong.log.gather.service.ApiLogService;
import com.zhong.log.gather.vo.BaseLogAggPageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhongyumin
 * @date 2021/3/21-下午1:33
 */
@RestController
@RequestMapping("api")
@Slf4j
@Api(value = "api日志",tags = {"api日志控制器"})
public class ApiLogController {
    @Autowired
    ApiLogService apiLogService;
    @GetMapping("get")
    @ApiOperation("api日志查询")
    public BaseLogAggPageVO search(ApiLogQueryDto logMsgQueryDto){
        long startTime = System.currentTimeMillis();
        log.info("api日志查询请求参数:{}", JSON.toJSONStringWithDateFormat(logMsgQueryDto,"yyyy-MM-dd HH:mm:ss"));
        BaseLogAggPageVO baseLogAggPageVO = apiLogService.searchByQuery(logMsgQueryDto);
        log.info("api日志查询请求耗时:{}ms",System.currentTimeMillis()-startTime);
        return baseLogAggPageVO;
    }
    @ApiOperation(value = "api日志删除",notes="日志删除接口不传参数默认删除所有应用三天前的DEBUG,INFO,TRACE日志")
    @DeleteMapping("delete")
    public String delete(BaseLogDeleteDto baseLogDeleteDto){
        long startTime = System.currentTimeMillis();
        log.info("api日志删除请求参数:{}", JSON.toJSONString(baseLogDeleteDto));
        apiLogService.deleteLog(baseLogDeleteDto);
        log.info("api日志删除请求耗时:{}ms",System.currentTimeMillis()-startTime);
        return "操作成功";
    }
}
