package com.zhong.log.gather.dao.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.elasticsearch.core.convert.DefaultElasticsearchTypeMapper;
import org.springframework.data.elasticsearch.core.convert.ElasticsearchConverter;
import org.springframework.data.elasticsearch.core.convert.MappingElasticsearchConverter;
import org.springframework.data.elasticsearch.core.mapping.SimpleElasticsearchMappingContext;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.lang.reflect.Field;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午11:02
 */
@Configuration
@EnableElasticsearchRepositories
@DependsOn
public class ElasticsearchRepositoryConfig {
    @Bean
    public ElasticsearchConverter elasticsearchConverter(SimpleElasticsearchMappingContext mappingContext) {
        MappingElasticsearchConverter mappingElasticsearchConverter = new MappingElasticsearchConverter(mappingContext);
        try {
            //去除"_class"属性
            Field typeMapper = MappingElasticsearchConverter.class.getDeclaredField("typeMapper");
            typeMapper.setAccessible(true);
            typeMapper.set(mappingElasticsearchConverter, new DefaultElasticsearchTypeMapper(null, mappingContext));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mappingElasticsearchConverter;
    }
}
