package com.zhong.log.gather.service;

import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.query.LogDeleteDto;

import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:10
 */
public interface LogService {
    void batchInsert(LogType logType, List<LogMessage> messages);
    void deleteLog(LogDeleteDto logDeleteDto);
}
