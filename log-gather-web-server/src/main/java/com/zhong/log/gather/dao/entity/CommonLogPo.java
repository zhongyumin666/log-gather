package com.zhong.log.gather.dao.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午10:14
 */
@Getter
@Setter
public class CommonLogPo {
    @Id
    private Long id;
    /**
     * 环境
     */
    @Field(type= FieldType.Keyword)
    private String environment;
    /**
     * 应用ID
     */
    @Field(type= FieldType.Keyword)
    private String applicationId;
    /**
     * 应用名称
     */
    @Field(type=FieldType.Keyword)
    private String applicationName;
    /**
     * 应用版本
     */
    @Field(type=FieldType.Keyword)
    private String applicationVersion;
    /**
     * 应用描述
     */
    @Field(type=FieldType.Keyword)
    private String applicationDescription;
    /**
     * 应用的主机
     */

    @Field(type=FieldType.Keyword)
    private String applicationHost;
    /**
     * 应用端口
     */

    @Field(type=FieldType.Keyword)
    private String applicationPort;
    /**
     * 日志等级
     */
    @Field(type=FieldType.Keyword)
    private String logLevel;
    /**
     * 时间戳
     */
    @Field(type=FieldType.Date,format = DateFormat.date_time)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date timeStamp;
}
