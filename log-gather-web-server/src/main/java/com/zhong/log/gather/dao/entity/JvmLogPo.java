package com.zhong.log.gather.dao.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午10:17
 */
@Document(indexName = "jvm_log")
@Getter
@Setter
public class JvmLogPo extends CommonLogPo  {
    /**
     * 收集次数
     */
    @Field(type= FieldType.Integer)
    Integer gatherCount;
    /**
     * jvm日志数据
     */
    @Field(type=FieldType.Text)
    String data;
}
