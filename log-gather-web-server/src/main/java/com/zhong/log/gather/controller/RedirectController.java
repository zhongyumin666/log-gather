package com.zhong.log.gather.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author zhongyumin
 * @date 2021/1/28-上午9:47
 */
@Controller
public class RedirectController {
    @GetMapping("/")
    public String redirect(){
        return "redirect:index.html";
    }
}
