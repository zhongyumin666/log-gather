package com.zhong.log.gather.consumer.manager;

/**
 * @author zhongyumin
 * @date 2021/3/3-下午3:55
 */
public interface MqConsumerManager {
    /**
     * 启动消费者
     */
    void  start() throws  Exception;

    /**
     * 关闭消费者
     */
    void  shutdown()throws  Exception;
}
