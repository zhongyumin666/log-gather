package com.zhong.log.gather.vo;

import com.zhong.log.gather.dao.entity.ApplicationLogPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhongyumin
 * @date 2021/1/14-下午9:34
 */
@Data
@EqualsAndHashCode
public class ApplicationLogVO extends ApplicationLogPo {

}
