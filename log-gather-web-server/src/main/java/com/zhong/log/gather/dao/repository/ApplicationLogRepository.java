package com.zhong.log.gather.dao.repository;

import com.zhong.log.gather.dao.entity.ApplicationLogPo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午11:51
 */
@Repository
public interface ApplicationLogRepository extends ElasticsearchRepository<ApplicationLogPo,Long> {
}
