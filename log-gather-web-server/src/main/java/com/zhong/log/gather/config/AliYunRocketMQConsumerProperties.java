package com.zhong.log.gather.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zhongyumin
 * @date 2021/7/6-下午10:20
 */
@ConfigurationProperties(prefix = "log.gather.consumer.aliyun.rocket")
@ConditionalOnProperty(prefix = "log.gather.consumer.aliyun.rocket", name = "enable", havingValue = "true")
@Component
@Setter
@Getter
public class AliYunRocketMQConsumerProperties {
    private boolean enable = false;
    //topic
    private String topic="LOG_GATHER";
    /**
     * rocketMq name server地址
     */
    private String nameSrvAddr;
    /**
     * AccessKey, 用于标识、校验用户身份(阿里云rocketMq用到)
     */
    private String accessKey;

    /**
     * SecretKey, 用于标识、校验用户身份(阿里云rocketMq用到)
     */
    private String secretKey;
    /**
     * 默认消费者数量,
     */
    private int count = 1;
    /**
     * 消费超时时间
     */
    private long consumeTimeout = 5000L;
    /**
     * 最小消费线程
     */
    private int consumeThreadMin = 1;
    /**
     * 最大消费线程(当前处理器的数量)
     */
    private int consumeThreadMax = Runtime.getRuntime().availableProcessors() / 4 + 1;
    /**
     * 消费失败最大重新消费次数
     */
    private int maxReconsumeTimes = 1;
    /**
     * 每次批量拉去的大小 阿里的实例 real size <=32  如果是apacheMq  <n n可以任意值
     */
    private int pullBatchSize = 32;
    /**
     * 批量拉取间隔
     */
    private long pullInterval = 0L;
}
