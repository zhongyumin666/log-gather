package com.zhong.log.gather.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.dto.LogMessageDto;
import com.zhong.log.gather.dao.entity.ApplicationLogPo;
import com.zhong.log.gather.dao.repository.ApplicationLogRepository;
import com.zhong.log.gather.query.ApplicationLogQueryDto;
import com.zhong.log.gather.query.BaseLogDeleteDto;
import com.zhong.log.gather.service.ApplicationLogService;
import com.zhong.log.gather.util.EsUtils;
import com.zhong.log.gather.util.IdAutoGainCtr;
import com.zhong.log.gather.vo.ApplicationLogVO;
import com.zhong.log.gather.vo.BaseLogAggPageVO;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.*;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午11:55
 */
@Service
public class ApplicationLogServiceImpl implements ApplicationLogService {
    @Autowired
    ApplicationLogRepository applicationLogRepository;
    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;
    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Override
    public void insert(LogMessageDto logMessage) {
        ApplicationLogPo applicationLogPo = JSON.parseObject(JSON.toJSONString(logMessage), ApplicationLogPo.class);
        applicationLogPo.setId(IdAutoGainCtr.getNextId());
        elasticsearchRestTemplate.save(applicationLogPo);
    }

    @Override
    public void batchInsert(List<LogMessageDto> messages) {
        List<ApplicationLogPo> applicationLogPos = JSON.parseArray(JSON.toJSONString(messages), ApplicationLogPo.class);
        applicationLogPos.forEach(item->item.setId(IdAutoGainCtr.getNextId()));
        elasticsearchRestTemplate.save(applicationLogPos);
    }

    @Override
    public BaseLogAggPageVO searchByQuery(ApplicationLogQueryDto queryDto)  {
        NativeSearchQueryBuilder nsb=new   NativeSearchQueryBuilder();
        Map<String,Object> filterFiled=new HashMap<>();
        filterFiled.put("environment",queryDto.getEnvironment());
        filterFiled.put("applicationVersion",queryDto.getApplicationVersion());
        filterFiled.put("applicationDescription",queryDto.getApplicationDescription());
        filterFiled.put("logLevel",queryDto.getLogLevel());
        filterFiled.put("threadName",queryDto.getThreadName());
        BoolQueryBuilder filter = EsUtils.buildBoolTermsConditions(filterFiled);
        Map<String,Object> queryFiled=new HashMap<>();
        queryFiled.put("applicationName",queryDto.getApplicationName());
        queryFiled.put("applicationHost",queryDto.getHost());
        queryFiled.put("applicationPort",queryDto.getPort());
        BoolQueryBuilder query = EsUtils.buildBoolTermsConditions(queryFiled);
        if(!StringUtils.isEmpty(queryDto.getMessage())){
            MultiMatchQueryBuilder   matchQueryBuilder=new MultiMatchQueryBuilder(queryDto.getMessage(),"message","callerData");
            matchQueryBuilder.analyzer("ik_max_word");
            matchQueryBuilder.minimumShouldMatch("75%");
            query.must(matchQueryBuilder);
        }
        NativeSearchQuery nativeSearchQuery=new NativeSearchQuery(query,filter);
        filter.must(new RangeQueryBuilder("timeStamp").gt(queryDto.getStartTime()).lt(queryDto.getEndTime()));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("environment",5,"environment"));
//        nativeSearchQuery.addAggregation(EsUtils.buildAgg("applicationId",10,"applicationId"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("applicationName",10,"applicationName"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("logLevel",6,"logLevel"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("host",10,"applicationHost"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("port",10,"applicationPort"));
        //设置查询全部日志,不然最多只会查10000条
        if(!StringUtils.isEmpty(queryDto.getApplicationName())){
            nativeSearchQuery.setTrackTotalHits(true);
        }
        nativeSearchQuery.setPageable(PageRequest.of(queryDto.getPageNo()-1,queryDto.getPageSize(),Sort.by("timeStamp").descending()));
        SearchHits<ApplicationLogPo> search = elasticsearchRestTemplate.search(nativeSearchQuery, ApplicationLogPo.class);
        SearchPage<ApplicationLogPo> searchHits = SearchHitSupport.searchPageFor(search, null);
        Page page = (Page) SearchHitSupport.unwrapSearchHits(searchHits);
        List<SearchHit> list = page.toList();
        List<ApplicationLogVO> vos = list.stream().map(item->(ApplicationLogPo)item.getContent()).map(item -> {
            ApplicationLogVO applicationLogVo = JSON.parseObject(JSON.toJSONString(item), ApplicationLogVO.class);
            return applicationLogVo;
        }).collect(Collectors.toList());
        Aggregations aggregations = search.getAggregations();
        //构建聚合条件
        Map<String, List<String>> aggSut = EsUtils.getAggSut(search.getAggregations());
        return new BaseLogAggPageVO(page.getTotalElements(),vos,aggSut);
    }
    @Override
    public void deleteLog(BaseLogDeleteDto deleteDto) {
        Map<String,Object> filterFiled=new HashMap<>();
        filterFiled.put("environment",deleteDto.getEnvironment());
        filterFiled.put("applicationName",deleteDto.getApplicationName());
        filterFiled.put("logLevel",deleteDto.getLogLevels());
        filterFiled.put("applicationHost",deleteDto.getHost());
        filterFiled.put("applicationPort",deleteDto.getPort());
        filterFiled.put("applicationVersion",deleteDto.getApplicationVersion());
        filterFiled.put("applicationDescription",deleteDto.getApplicationDescription());
        BoolQueryBuilder query = EsUtils.buildBoolTermsConditions(filterFiled);
        Date startTime=null;
        Date endTime=null;
        if(!StringUtils.isEmpty(deleteDto.getStartTime())){
            startTime=DateUtil.parseDateTime(deleteDto.getStartTime()).toJdkDate();
        }
        if(!StringUtils.isEmpty(deleteDto.getEndTime())){
            endTime=DateUtil.parseDateTime(deleteDto.getEndTime()).toJdkDate();
        };
        query.must(new RangeQueryBuilder("timeStamp")
                .gt(startTime)
                .lt(endTime));
        NativeSearchQuery nativeSearchQuery=new NativeSearchQuery(query,null);
        elasticsearchRestTemplate.delete(nativeSearchQuery,ApplicationLogPo.class,IndexCoordinates.of("application_log"));
        elasticsearchRestTemplate.indexOps(ApplicationLogPo.class).refresh();
    }

    @Override
    public void deleteAllLog() {
        MatchAllQueryBuilder queryBuilder=new MatchAllQueryBuilder();
        NativeSearchQuery nativeSearchQuery=new NativeSearchQuery(queryBuilder,null);
        elasticsearchRestTemplate.delete(nativeSearchQuery,ApplicationLogPo.class,IndexCoordinates.of("application_log"));
    }
}
