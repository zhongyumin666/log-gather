package com.zhong.log.gather.service.impl;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.dto.ApiMessageDto;
import com.zhong.log.gather.common.dto.JvmMessageDto;
import com.zhong.log.gather.common.dto.LogMessageDto;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.query.LogDeleteDto;
import com.zhong.log.gather.service.ApiLogService;
import com.zhong.log.gather.service.ApplicationLogService;
import com.zhong.log.gather.service.JvmLogService;
import com.zhong.log.gather.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:10
 */
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    JvmLogService jvmLogService;
    @Autowired
    ApiLogService apiLogService;
    @Autowired
    ApplicationLogService applicationLogService;

    @Override
    public void batchInsert(LogType logType, List<LogMessage> messages) {
        if (CollectionUtils.isEmpty(messages)) return;
        switch (logType) {
            case APPLICATION:
                applicationLogService.batchInsert(JSON.parseArray(JSON.toJSONString(messages), LogMessageDto.class));
                break;
            case API:
                apiLogService.batchInsert(JSON.parseArray(JSON.toJSONString(messages), ApiMessageDto.class));
                break;
            case JVM:
                jvmLogService.batchInsert(JSON.parseArray(JSON.toJSONString(messages), JvmMessageDto.class));
                break;
        }
    }

    @Override
    public void deleteLog(LogDeleteDto logDeleteDto) {
        LogType logType = logDeleteDto.getLogType();
        switch (logType) {
            case APPLICATION:
                applicationLogService.deleteLog(logDeleteDto);
                break;
            case API:
                apiLogService.deleteLog(logDeleteDto);
                break;
            case JVM:
                jvmLogService.deleteLog(logDeleteDto);
                break;
            default:
                throw new RuntimeException("不支持此种日志类型的删除!");
        }

    }
}
