package com.zhong.log.gather.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zhongyumin
 * @date 2021/3/3-下午4:07
 */
@ConfigurationProperties(prefix = "log.gather.consumer.apache.rocket")
@ConditionalOnProperty(prefix = "log.gather.consumer.apache.rocket", name = "enable", havingValue = "true")
@Component
@Setter
@Getter
public class ApacheRocketMQConsumerProperties {
    private boolean enable = false;
    /**
     * rocketMq name server地址
     */
    private String nameSrvAddr;
    //topic
    private String topic="LOG_GATHER";
    /**
     * 默认消费者数量,
     */
    private int count = 1;
    /**
     * 消费超时时间
     */
    private long consumeTimeout = 5000L;
    /**
     * 最小消费线程
     */
    private int consumeThreadMin = 1;
    /**
     * 最大消费线程(当前处理器的数量)
     */
    private int consumeThreadMax = Runtime.getRuntime().availableProcessors() / 4 + 1;
    /**
     * 消费失败最大重新消费次数
     */
    private int maxReconsumeTimes = 1;
    /**
     * 每次批量拉去的大小 阿里的实例 real size <=32  如果是apacheMq  <n n可以任意值
     */
    private int pullBatchSize = 500;
    /**
     * 批量拉取间隔
     */
    private long pullInterval = 0L;
}
