package com.zhong.log.gather.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhongyumin
 * @date 2021/3/22-下午10:17
 */
@Getter
@Setter
@ApiModel("基础查询参数")
public class BaseLogQueryDto {
    //应用id
    @ApiModelProperty(value = "应用环境",example = "pro")
    private String environment;
    //应用id
    @ApiModelProperty(value = "应用id",example = "0")
    private String applicationId;
    //应用名字
    @ApiModelProperty(value = "应用名字",example = "log-gather-web-test")
    private String applicationName;
    //应用版本
    @ApiModelProperty(value = "应用版本",example = "1.0")
    private String applicationVersion;
    //应用描述
    @ApiModelProperty(value = "应用描述",example = "日志查询测试服务")
    private String applicationDescription;
    //日志等级
    @ApiModelProperty(value = "日志等级",example = "INFO")
    private String logLevel;
    //机器地址
    @ApiModelProperty(value = "机器地址",example = "127.0.0.1")
    private String host;
    //机器端口
    @ApiModelProperty(value = "机器端口",example = "机器端口")
    private String port;
}
