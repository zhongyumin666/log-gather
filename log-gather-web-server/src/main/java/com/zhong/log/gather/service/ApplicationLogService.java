package com.zhong.log.gather.service;

import com.zhong.log.gather.common.dto.LogMessageDto;
import com.zhong.log.gather.query.ApplicationLogQueryDto;
import com.zhong.log.gather.query.BaseLogDeleteDto;
import com.zhong.log.gather.vo.BaseLogAggPageVO;

import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午11:52
 */
public interface ApplicationLogService {
    void insert(LogMessageDto logMessage);
    void batchInsert(List<LogMessageDto> messages);
    BaseLogAggPageVO searchByQuery(ApplicationLogQueryDto queryDto) ;
    void deleteLog(BaseLogDeleteDto baseLogDeleteDto);
    void deleteAllLog();
//    void deleteById(Long id);
//    /**
//     * 删除时间戳之前的数据
//     */
//    void deleteByTimeStamp();
}
