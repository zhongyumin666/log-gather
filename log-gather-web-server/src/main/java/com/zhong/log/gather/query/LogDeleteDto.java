package com.zhong.log.gather.query;

import com.zhong.log.gather.common.enums.LogType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhongyumin
 * @date 2021/3/23-上午10:53
 */
@Setter
@Getter
public class LogDeleteDto extends BaseLogDeleteDto {
    @ApiModelProperty(value = "日志类型",example = "JVM/API/APPLICATION")
    private LogType logType;
}
