package com.zhong.log.gather.dao.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:04
 */
@Data
@Document(indexName = "api_log")
public class ApiLogPo extends CommonLogPo{
    /**
     * api名称(className+methodName)
     */
    @Field(type= FieldType.Text,analyzer = "ik_max_word")
    private String apiName;
    /**
     * 方法
     */
    @Field(type=FieldType.Keyword)
    private String method;
    /**
     * 累计调用数
     */
    @Field(type=FieldType.Long)
    private Long callCount;
    /**
     * uri
     */
    @Field(type=FieldType.Text,analyzer = "ik_max_word")
    private String uri;

    /**
     * 调用时间
     */
    @Field(type=FieldType.Long)
    private Long callTime;
    /**
     * 查询参数
     */
    @Field(type=FieldType.Text,analyzer = "ik_max_word")
    private String queryParam;
}
