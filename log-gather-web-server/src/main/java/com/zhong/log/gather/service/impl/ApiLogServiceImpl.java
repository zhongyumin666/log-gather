package com.zhong.log.gather.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.dto.ApiMessageDto;
import com.zhong.log.gather.dao.entity.ApiLogPo;
import com.zhong.log.gather.dao.repository.ApiLogRepository;
import com.zhong.log.gather.query.ApiLogQueryDto;
import com.zhong.log.gather.query.BaseLogDeleteDto;
import com.zhong.log.gather.service.ApiLogService;
import com.zhong.log.gather.util.EsUtils;
import com.zhong.log.gather.util.IdAutoGainCtr;
import com.zhong.log.gather.vo.BaseLogAggPageVO;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.*;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:17
 */
@Service
public class ApiLogServiceImpl implements ApiLogService {
    @Autowired
    ApiLogRepository apiLogRepository;
    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;
    @Override
    public void batchInsert(List<ApiMessageDto> messages) {
        List<ApiLogPo> apiLogPos = JSON.parseArray(JSON.toJSONString(messages), ApiLogPo.class);
        apiLogPos.forEach(item->item.setId(IdAutoGainCtr.getNextId()));
        elasticsearchRestTemplate.save(apiLogPos);
    }

    @Override
    public BaseLogAggPageVO searchByQuery(ApiLogQueryDto queryDto) {
        Map<String,Object> filterFiled=new HashMap<>();
        filterFiled.put("environment",queryDto.getEnvironment());
        filterFiled.put("method",queryDto.getMethod());
        filterFiled.put("applicationVersion",queryDto.getApplicationVersion());
        filterFiled.put("applicationDescription",queryDto.getApplicationDescription());
        filterFiled.put("logLevel",queryDto.getLogLevel());
        BoolQueryBuilder filter = EsUtils.buildBoolTermsConditions(filterFiled);
        Map<String,Object> queryFiled=new HashMap<>();
        queryFiled.put("applicationName",queryDto.getApplicationName());
        queryFiled.put("applicationHost",queryDto.getHost());
        queryFiled.put("applicationPort",queryDto.getPort());
        BoolQueryBuilder query = EsUtils.buildBoolTermsConditions(queryFiled);
        if(!StringUtils.isEmpty(queryDto.getMessage())){
            MultiMatchQueryBuilder matchQueryBuilder=new MultiMatchQueryBuilder(queryDto.getMessage(),"apiName","uri");
            matchQueryBuilder.analyzer("ik_smart");
            matchQueryBuilder.minimumShouldMatch("65%");
            query.must(matchQueryBuilder);
        }
        NativeSearchQuery nativeSearchQuery=new NativeSearchQuery(query,filter);
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("environment",5,"environment"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("method",5,"method"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("applicationName",10,"applicationName"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("logLevel",8,"logLevel"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("host",10,"applicationHost"));
        nativeSearchQuery.addAggregation(EsUtils.buildAgg("port",10,"applicationPort"));
        //设置查询全部日志,不然最多只会查10000条
        if(!StringUtils.isEmpty(queryDto.getApplicationName())){
            nativeSearchQuery.setTrackTotalHits(true);
        }
        nativeSearchQuery.setPageable(PageRequest.of(queryDto.getPageNo()-1,queryDto.getPageSize(), Sort.by("timeStamp").descending()));
        SearchHits<ApiLogPo> search = elasticsearchRestTemplate.search(nativeSearchQuery, ApiLogPo.class);
        SearchPage<ApiLogPo> searchHits = SearchHitSupport.searchPageFor(search, null);
        Page page = (Page) SearchHitSupport.unwrapSearchHits(searchHits);
        List<SearchHit> list = page.toList();
        List<ApiLogPo> vos = list.stream().map(item->(ApiLogPo)item.getContent()).map(item -> {
            ApiLogPo apiLogPo = JSON.parseObject(JSON.toJSONString(item), ApiLogPo.class);
            return apiLogPo;
        }).collect(Collectors.toList());
        Aggregations aggregations = search.getAggregations();
        //构建聚合条件
        Map<String, List<String>> aggSut = EsUtils.getAggSut(search.getAggregations());
        return new BaseLogAggPageVO(page.getTotalElements(),vos,aggSut);
    }

    @Override
    public void deleteLog(BaseLogDeleteDto deleteDto) {
        Map<String,Object> filterFiled=new HashMap<>();
        filterFiled.put("environment",deleteDto.getEnvironment());
        filterFiled.put("applicationName",deleteDto.getApplicationName());
        filterFiled.put("logLevel",deleteDto.getLogLevels().toArray());
        filterFiled.put("applicationHost",deleteDto.getHost());
        filterFiled.put("applicationPort",deleteDto.getPort());
        filterFiled.put("applicationVersion",deleteDto.getApplicationVersion());
        filterFiled.put("applicationDescription",deleteDto.getApplicationDescription());
        BoolQueryBuilder query = EsUtils.buildBoolTermsConditions(filterFiled);
        Date startTime=null;
        Date endTime=null;
        if(!StringUtils.isEmpty(deleteDto.getStartTime())){
            startTime=DateUtil.parseDateTime(deleteDto.getStartTime()).toJdkDate();
        }
        if(!StringUtils.isEmpty(deleteDto.getEndTime())){
            endTime=DateUtil.parseDateTime(deleteDto.getEndTime()).toJdkDate();
        };
        query.must(new RangeQueryBuilder("timeStamp")
                .gt(startTime)
                .lt(endTime));
        NativeSearchQuery nativeSearchQuery=new NativeSearchQuery(query,null);
        elasticsearchRestTemplate.delete(nativeSearchQuery, ApiLogPo.class,IndexCoordinates.of("api_log"));
        elasticsearchRestTemplate.indexOps(ApiLogPo.class).refresh();
    }
}
