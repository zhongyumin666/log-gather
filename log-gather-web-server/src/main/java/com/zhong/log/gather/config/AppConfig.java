package com.zhong.log.gather.config;

import com.zhong.log.gather.consumer.adapter.MessageSaveAdapter;
import com.zhong.log.gather.consumer.adapter.impl.MessageSaveAdapterImpl;
import com.zhong.log.gather.consumer.manager.MqConsumerManager;
import com.zhong.log.gather.consumer.manager.impl.AliYunRocketMqConsumerManagerImpl;
import com.zhong.log.gather.consumer.manager.impl.ApacheRocketMqConsumerManagerImpl;
import com.zhong.log.gather.consumer.manager.impl.KafkaConsumerManagerImpl;
import com.zhong.log.gather.util.IdAutoGainCtr;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午11:08
 */
@Configuration
public class AppConfig{

    @Bean
    public IdAutoGainCtr idAutoGainCtr(){
        return new IdAutoGainCtr();
    }

    @Bean
    @ConditionalOnBean(AliYunRocketMQConsumerProperties.class)
    MqConsumerManager aliYunRocketMQConsumer() {
        return new AliYunRocketMqConsumerManagerImpl();
    }

    @Bean
    @ConditionalOnBean(ApacheRocketMQConsumerProperties.class)
    MqConsumerManager apacheRocketMQConsumer() {
        return new ApacheRocketMqConsumerManagerImpl();
    }
    @Bean
    @ConditionalOnBean(KafkaConsumerProperties.class)
    MqConsumerManager kafkaConsumer() {
        return new KafkaConsumerManagerImpl();
    }

    @Bean
    MessageSaveAdapter messageSaveAdapter() {
        return new MessageSaveAdapterImpl();
    }
}
