package com.zhong.log.gather.consumer.manager.impl;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.config.KafkaConsumerProperties;
import com.zhong.log.gather.consumer.adapter.MessageSaveAdapter;
import com.zhong.log.gather.consumer.manager.MqConsumerManager;
import com.zhong.log.gather.exception.MessageSaveAdapterException;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.util.*;

/**
 * @author zhongyumin
 * @date 2021/7/6-下午10:58
 */
@Slf4j
public class KafkaConsumerManagerImpl implements MqConsumerManager, InitializingBean, DisposableBean,Runnable {
    @Autowired
    private  KafkaConsumerProperties kafkaConsumerProperties;
    @Autowired
    MessageSaveAdapter messageSaveAdapter;
    KafkaConsumer kafkaConsumer;
    /**
     * 线程
     */
    Thread thread;
    /**
     * 退出拉取贡献变量
     */
    private  volatile boolean  stop=false;
    @Override
    public void start() throws Exception {
        Properties props = new Properties();

        // 必须设置的属性
        props.put("bootstrap.servers", kafkaConsumerProperties.getBootstrapServers());
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("group.id", kafkaConsumerProperties.getGroupId());
        props.put("group.instance.id", kafkaConsumerProperties.getGroupInstanceId());

        // 可选设置属性
        props.put("enable.auto.commit",kafkaConsumerProperties.isEnableAutoCommit());
        // 自动提交offset
        props.put("auto.commit.interval.ms", kafkaConsumerProperties.getAutoCommitIntervalMs());
        props.put("auto.offset.reset",kafkaConsumerProperties.getAutoOffsetReset());
        props.put("fetch.max.bytes",kafkaConsumerProperties.getFetchMaxBytes());
        props.put("fetch.max.wait.ms",kafkaConsumerProperties.getFetchMaxWaitMs());
        props.put("fetch.min.bytes",kafkaConsumerProperties.getFetchMinBytes());
        props.put("max.partition.fetch.bytes",kafkaConsumerProperties.getMaxPartitionFetchBytes());
        props.put("max.poll.records",kafkaConsumerProperties.getMaxPollRecords());
        props.put("max.poll.interval.ms",kafkaConsumerProperties.getMaxPollIntervalMs());
        props.put("session.timeout.ms",kafkaConsumerProperties.getSessionTimeoutMs());
        props.put("heart.beat.interval.ms",kafkaConsumerProperties.getHeartBeatIntervalMs());
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        // 订阅topic
        consumer.subscribe(Collections.singletonList(kafkaConsumerProperties.getTopic()));
        this.kafkaConsumer=consumer;
        this.thread=new Thread(this);
        this.thread.setDaemon(true);
        this.thread.setName("kafkaConsumer");
        this.thread.start();
        log.info("kafka 消费者启动成功");
    }

    @Override
    public void shutdown() throws Exception {
        this.stop=true;
        this.thread.interrupt();
        this.thread.join();
        log.info("kafka 消费者关闭成功");
    }

    @Override
    public void destroy() throws Exception {
        this.shutdown();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
       this.start();
    }

    @Override
    public void run() {
        while(!stop) {
            try {
                //  从服务器开始拉取数据
                ConsumerRecords<String, String> records = this.kafkaConsumer.poll(Duration.ofMillis(200));
                Iterator<ConsumerRecord<String, String>> iterator = records.iterator();
                List<LogMessage> result=new ArrayList<>(records.count());
                while (iterator.hasNext()){
                    result.add(JSON.parseObject(iterator.next().value(),LogMessage.class));
                }
                if(result.size()>0){
                    try {
                        messageSaveAdapter.submit(result);
                    }catch (MessageSaveAdapterException e){
                        //适配器停止,消费者也停止拉取
                        this.shutdown();
                        log.warn("适配器已经关闭运行,kafka消费者退出--->",e);
                    }
                }
            } catch (Exception e) {
                log.warn("KafkaConsumer消费发生异常",e);
            }
        }
        //关闭消费者
        kafkaConsumer.close();
    }
}
