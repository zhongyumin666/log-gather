package com.zhong.log.gather.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author zhongyumin
 * @date 2021/1/25-下午5:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseLogAggPageVO<T> {
    private Long total;
    private List<T> data;
    private Map<String,List<String>> agg;
}
