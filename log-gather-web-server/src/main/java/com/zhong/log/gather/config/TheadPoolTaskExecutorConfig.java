package com.zhong.log.gather.config;

import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.boot.task.TaskExecutorCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author zhongyumin
 * @date 2021/7/6-下午9:23
 */
@Configuration
public class TheadPoolTaskExecutorConfig implements TaskExecutorCustomizer {
    @Override
    public void customize(ThreadPoolTaskExecutor taskExecutor) {
        taskExecutor.setCorePoolSize(10);
        taskExecutor.setMaxPoolSize(20);
        taskExecutor.setQueueCapacity(1000);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setAllowCoreThreadTimeOut(true);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setAwaitTerminationSeconds(60);
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    }
    @Bean
    public ThreadPoolTaskExecutor messageSaveAdapterExecutor(TaskExecutorBuilder builder){
        ThreadPoolTaskExecutor executor = builder.build();
        executor.setQueueCapacity(500);
        return executor;
    }
}
