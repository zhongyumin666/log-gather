package com.zhong.log.gather.consumer.adapter.impl;

import com.zhong.log.gather.common.domain.LogMessage;
import com.zhong.log.gather.common.enums.LogType;
import com.zhong.log.gather.consumer.adapter.MessageSaveAdapter;
import com.zhong.log.gather.exception.MessageSaveAdapterException;
import com.zhong.log.gather.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 消息适配层(将消息集中合并插入消费)
 *
 * @author zhongyumin
 * @date 2021/2/24-下午3:47
 */
@Slf4j
public class MessageSaveAdapterImpl implements MessageSaveAdapter, InitializingBean, DisposableBean {
    /**
     * 与最大延迟时间是相互影响的
     */
    @Value("${log.consumer.adapter.min-submit-size:100}")
    private Integer minSubmitSize;
    @Value("${log.consumer.adapter.max-delay:2000}")
    private Integer maxDelay;
    @Value("${log.consumer.adapter.queueSize:2000}")
    private Integer queueSize;
    @Autowired
    @Qualifier("messageSaveAdapterExecutor")
    private ThreadPoolTaskExecutor executorService;
    @Autowired
    private LogService logService;

    private Map<LogType, LoggerAdapterWorker> workers = new HashMap<>();

    private volatile boolean stop = false;

    @Override
    public void submit(List<LogMessage> messages) {
        if (stop) throw new MessageSaveAdapterException("适配器已经停止运行");
        messages.forEach(item -> {
            workers.get(item.getLogType()).submit(item);
        });
    }

    @Override
    public void start() {
        workers.put(LogType.APPLICATION, createLoggerAdapterWorker(LogType.APPLICATION, "APPLICATION-ADAPTER-WORKER"));
        workers.put(LogType.API, createLoggerAdapterWorker(LogType.API, "API-ADAPTER-WORKER"));
        log.info("API日志消息适配线程启动成功----->");
        workers.put(LogType.JVM, createLoggerAdapterWorker(LogType.JVM, "JVM-ADAPTER-WORKER"));
        log.info("JVM日志消息适配线程启动成功----->");
        workers.values().forEach(worker -> {
            worker.startUp();
        });
    }

    private LoggerAdapterWorker createLoggerAdapterWorker(LogType logType, String name) {
        return new LoggerAdapterWorker(LogType.APPLICATION, name, queueSize, minSubmitSize, maxDelay, executorService, logService);
    }

    @Override
    public void stop() {
        stop = true;
        workers.values().forEach(worker -> {
            try {
                worker.shutdown();
                worker.interrupt();
                worker.join();
            } catch (InterruptedException e) {
                log.error("适配线程{}停止异常", worker.getName(), e);
            }
        });
        log.info("全部适配线程已经停止------>");
    }

    @Override
    public void destroy() {
        this.stop();
    }

    @Override
    public void afterPropertiesSet() {
        this.start();
    }

}
