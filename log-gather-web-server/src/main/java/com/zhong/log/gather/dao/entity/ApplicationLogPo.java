package com.zhong.log.gather.dao.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author zhongyumin
 * @date 2021/1/14-上午10:57
 */
@Data
@Document(indexName = "application_log")
public class ApplicationLogPo extends CommonLogPo{
    /**
     * 日志名称
     */
    @Field(type=FieldType.Keyword)
    private String loggerName;
    /**
     * 调用线程
     */
    @Field(type=FieldType.Keyword)
    private String threadName;

    /**
     * 消息
     */
    @Field(type=FieldType.Text,analyzer="ik_max_word")
    private String message;
    /**
     * 调用盏
     */
    @Field(type=FieldType.Text,analyzer="ik_max_word")
    private String callerData;

}
