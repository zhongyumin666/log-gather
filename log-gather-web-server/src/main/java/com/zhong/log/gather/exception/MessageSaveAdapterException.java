package com.zhong.log.gather.exception;

/**
 * @author zhongyumin
 * @date 2021/8/2-上午10:35
 */
public class MessageSaveAdapterException extends RuntimeException{
    public MessageSaveAdapterException(String message)
    {
        super(message);
    }
}
