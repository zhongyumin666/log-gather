package com.zhong.log.gather.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author zhongyumin
 * @date 2021/7/6-下午10:40
 */
@ConfigurationProperties(prefix = "log.gather.consumer.apache.kafka")
@ConditionalOnProperty(prefix = "log.gather.consumer.apache.kafka", name = "enable", havingValue = "true")
@Component
@Setter
@Getter
public class KafkaConsumerProperties {
    private boolean enable = false;
    //kafak broker 地址,多个用","隔开
    private String bootstrapServers;
    //topic
    private String topic="LOG_GATHER";

    /**
     * 从服务端抓取端最小字节数,默认配置1b
     */
    private int fetchMinBytes = 1;
    /**
     * 从服务端抓取端最大字节数,默认配置50M
     */
    private int fetchMaxBytes = 52428800;
    /**
     * 拉不到数据时最大的阻塞时间
     */
    private int fetchMaxWaitMs = 500;
    /**
     * 从分区抓取的最大字节数,和官网一致为1M
     */
    private Integer maxPartitionFetchBytes = 1048576;
    /**
     * @note 也是topic的名称
     */
    private String groupId = "LOG_GATHER_APACHE_KAFKA";


    private String groupInstanceId = UUID.randomUUID().toString();
    /**
     * 每次最大拉取的消息数量
     */
    private Integer maxPollRecords = 50;
    /**
     * 拉取间隔/默认1s
     */
    private Integer maxPollIntervalMs = 1000;
    /**
     * 会话超时/10s 和官网一致为10s
     */
    private Integer sessionTimeoutMs = 10000;
    /**
     * 心跳检测间隔/3s 和官网一致为3s   此值必须小于sessionTimeoutMs,官网建议为不超过其值的1/3
     */
    private Integer heartBeatIntervalMs = 3000;

    /**
     * 当偏移量不存在时采取的策略
     * 1:latest 使用最新的偏移量
     * 2:earliest 使用最晚的偏移量,到时可能重复消费,日志不需要
     * 3:none 抛异常
     */
    private String autoOffsetReset = "latest";

    /**
     * 自动提交/默认为true
     */
    private boolean enableAutoCommit = true;
    /**
     * 自动提交间隔 /每隔5s提交一次,consumer异常宕机可能重复消费,五秒之前的数据默认是已经消费成功的了
     */
    private int autoCommitIntervalMs = 5000;


}
