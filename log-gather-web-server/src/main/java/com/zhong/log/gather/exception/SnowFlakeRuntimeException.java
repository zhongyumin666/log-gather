package com.zhong.log.gather.exception;


public class SnowFlakeRuntimeException extends RuntimeException{
    public SnowFlakeRuntimeException(String message)
    {
        super(message);
    }
}
