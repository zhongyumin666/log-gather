package com.zhong.log.gather.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author zhongyumin
 * @date 2021/3/22-下午10:55
 */
@Getter
@Setter
@ApiModel("jvm查询参数")
public class JvmLogQueryDto extends BaseLogQueryDto{
    //开始时间
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    //结束时间
    @ApiModelProperty("结束时间")
    private Date endTime;
    @ApiModelProperty("每页大小")
    private int pageSize=10;
    @ApiModelProperty("页码")
    private int pageNo=1;
}
