package com.zhong.log.gather.service;

import com.zhong.log.gather.common.dto.ApiMessageDto;
import com.zhong.log.gather.query.ApiLogQueryDto;
import com.zhong.log.gather.query.BaseLogDeleteDto;
import com.zhong.log.gather.vo.BaseLogAggPageVO;

import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:13
 */
public interface ApiLogService {
    void batchInsert(List<ApiMessageDto> messages);
    BaseLogAggPageVO searchByQuery(ApiLogQueryDto queryDto) ;
    void deleteLog(BaseLogDeleteDto baseLogDeleteDto);
}
