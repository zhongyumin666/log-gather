package com.zhong.log.gather;

import org.slf4j.MDC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author zhongyumin
 * @date 2021/1/12-下午6:57
 */
@SpringBootApplication
@EnableDiscoveryClient(autoRegister = false)
@EnableScheduling
public class LogGatherWebServer {
    public static void main(String[] args) {

        SpringApplication.run(LogGatherWebServer.class,args);
    }
}
