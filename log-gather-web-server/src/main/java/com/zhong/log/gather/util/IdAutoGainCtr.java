package com.zhong.log.gather.util;

import com.alibaba.fastjson.JSON;
import com.zhong.log.gather.exception.SnowFlakeRuntimeException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * id自增控制器
 * @author zhongyumin
 * @date 2021/1/14-上午11:09
 */
@Slf4j
@ConfigurationProperties(prefix = "snow")
@Getter
@Setter
public class IdAutoGainCtr {
    List<Long> workers;
    private static List<SnowFlake> snowFlakes=null;
    public static long getNextId(){
        if(snowFlakes==null||snowFlakes.size()==0)throw new SnowFlakeRuntimeException("自增bean 还未初始化完成,请稍后在试!"+ LocalDateTime.now().toString());
        for (SnowFlake snowFlake:snowFlakes){
            try {
                return snowFlake.nextId();
            }catch (Exception e){
                log.info("发生时间回拨"+ JSON.toJSONString(snowFlake));
            }
        }
        throw new SnowFlakeRuntimeException("发生时间回拨！"+ LocalDateTime.now().toString());
    }

    @PostConstruct
    public void  init(){
        snowFlakes=new ArrayList<>();
        workers.forEach(id->{
            SnowFlake snowFlake = new SnowFlake(id);
            snowFlakes.add(snowFlake);
        });
        log.info("初始化雪花自增器成功-----");
    }
}
