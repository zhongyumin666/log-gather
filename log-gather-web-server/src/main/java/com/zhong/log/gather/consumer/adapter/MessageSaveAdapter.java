package com.zhong.log.gather.consumer.adapter;

import com.zhong.log.gather.common.domain.LogMessage;

import java.util.List;

/**
 * 消息适配层(将消息集中合并插入消费)
 * @author zhongyumin
 * @date 2021/2/24-下午3:40
 */
public interface MessageSaveAdapter{
     /**
     * 提交消息
     * @param messages
     */
     void submit(List<LogMessage> messages);

     /**
      * 启动适配器
      */
     void start();

     /**
      * 关闭适配器
      */
     void  stop();
}
