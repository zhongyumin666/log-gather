package com.zhong.log.gather.service;

import com.zhong.log.gather.common.dto.JvmMessageDto;
import com.zhong.log.gather.query.BaseLogDeleteDto;
import com.zhong.log.gather.query.JvmLogQueryDto;
import com.zhong.log.gather.vo.BaseLogAggPageVO;

import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/3/18-下午11:13
 */
public interface JvmLogService {
    void batchInsert(List<JvmMessageDto> messages);
    BaseLogAggPageVO searchByQuery(JvmLogQueryDto jvmLogQueryDto) ;
    void deleteLog(BaseLogDeleteDto baseLogDeleteDto);
}
