package com.zhong.log.gather.common.domain;

import com.zhong.log.gather.common.enums.LogTag;
import com.zhong.log.gather.common.enums.LogType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午1:48
 */
@Setter
@Getter
public class LogMessage {
    /**
     * 日志标签,表明是否是压缩的
     */
    private LogTag logTag;
    /**
     * 日志类型
     */
    private LogType logType;
    /**
     * 消息体
     */
    private byte[] message;
}
