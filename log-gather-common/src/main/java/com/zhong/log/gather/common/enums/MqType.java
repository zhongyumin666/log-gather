package com.zhong.log.gather.common.enums;

/**
 * @author zhongyumin
 * @date 2021/7/27-下午3:06
 */
public enum MqType {
    APACHE_KAFKA,APACHE_ROCKET_MQ,ALI_YUN_ROCKET_MQ,RABBIT_MQ
}
