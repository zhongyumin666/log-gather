package com.zhong.log.gather.common.dto;

import lombok.*;

import java.io.Serializable;

/**
 * @author zhongyumin
 * @date 2021/1/12-下午1:34
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogMessageDto extends MessageCommonDto implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 日志名称
     */
    private String loggerName;
    /**
     * 调用线程
     */
    private String threadName;
    /**
     * 消息
     */
    private String message;
    /**
     * 调用盏
     */
    private String callerData;
}
