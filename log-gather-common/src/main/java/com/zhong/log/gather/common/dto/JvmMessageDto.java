package com.zhong.log.gather.common.dto;

import lombok.*;

import java.io.Serializable;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午2:16
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JvmMessageDto extends MessageCommonDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 收集次数
     */
    Integer gatherCount;
    /**
     * jvm日志数据
     */
    String data;
}
