package com.zhong.log.gather.common.enums;

import lombok.Getter;

/**
 * 消息类型枚举
 * @author zhongyumin
 * @date 2021/3/3-下午3:13
 */
@Getter
public enum LogTag {
    COMMON(0,"COMMON"),GZIP(1,"GZIP");
    private String name;
    private int code;
    LogTag(int code, String name){
        this.code=code;
        this.name=name;
    }

}
