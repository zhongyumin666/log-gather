package com.zhong.log.gather.common.enums;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午7:11
 */
public enum LogLevel {
    DEBUG,TRACE,INFO,WARN,ERROR
}
