package com.zhong.log.gather.common.dto;

import lombok.*;

import java.io.Serializable;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午1:55
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApiMessageDto extends MessageCommonDto implements Serializable {
   private static final long serialVersionUID = 1L;
    /**
     * api名称(className+methodName)
     */
   private String apiName;
    /**
     * 方法
     */
   private String method;
    /**
     * 累计调用数
     */
   private Long callCount;
    /**
     * uri
     */
   private String uri;

    /**
     * 调用时间
     */
   private Long callTime;
    /**
     * 查询参数
     */
   private String queryParam;
}
