package com.zhong.log.gather.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author zhongyumin
 * @date 2021/3/3-上午12:22
 */
public class GzipUtil {

    public static byte[] zip(byte[] bytes) {
        ByteArrayOutputStream out = null;
        GZIPOutputStream zipOutputStream = null;
        try {
            out = new ByteArrayOutputStream();
            zipOutputStream = new GZIPOutputStream(out);
            zipOutputStream.write(bytes);
            zipOutputStream.finish();
            return out.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static byte[] unzip(byte[] str)  {
        ByteArrayOutputStream output =null;
        ByteArrayInputStream bint =null;
        GZIPInputStream gzipInputStream =null;
        try {
            bint = new ByteArrayInputStream(str);
            gzipInputStream = new GZIPInputStream(bint);
            output = new ByteArrayOutputStream();
            int len;
            byte[] buffer = new byte[1024];
            while ((len = gzipInputStream.read(buffer)) != -1) {
                output.write(buffer, 0, len);
            }
            output.flush();
            return output.toByteArray();
        }catch (IOException e){
            if(gzipInputStream!=null){
                try {
                    gzipInputStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if(bint!=null){
                try {
                    bint.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if(output!=null){
                try {
                    output.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
}
