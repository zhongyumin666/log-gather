package com.zhong.log.gather.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午1:56
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageCommonDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 环境
     */
    private String environment;
    /**
     * 应用ID
     */
    private String applicationId;
    /**
     * 应用名称
     */
    private String applicationName;
    /**
     * 应用版本
     */
    private String applicationVersion;
    /**
     * 应用描述
     */
    private String applicationDescription;
    /**
     * 应用的主机
     */
    private String applicationHost;
    /**
     * 应用端口
     */
    private String applicationPort;
    /**
     * 日志等级
     */
    private String logLevel;
    /**
     * 时间戳
     */
    private Long timeStamp;
}
