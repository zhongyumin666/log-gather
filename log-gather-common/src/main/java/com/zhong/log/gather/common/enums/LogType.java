package com.zhong.log.gather.common.enums;

import lombok.Getter;

/**
 * @author zhongyumin
 * @date 2021/3/17-下午1:48
 */
@Getter
public enum LogType {
    API(2,"api日志")
    ,JVM(1,"jvm日志")
    ,APPLICATION(0,"应用日志");
    private Integer code;
    private String name;
     LogType(Integer code,String name){
        this.code=code;
        this.name=name;
    }
}
