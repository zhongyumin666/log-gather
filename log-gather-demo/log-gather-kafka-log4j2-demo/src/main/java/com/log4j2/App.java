package com.log4j2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhongyumin
 * @date 2021/7/27-下午2:55
 */
@SpringBootApplication
@RestController
@Slf4j
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class,args);
    }
    @GetMapping("/test")
    public void test(){
        log.error("我是error异常啊");
    }
}
